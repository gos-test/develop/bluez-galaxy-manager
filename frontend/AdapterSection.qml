import QtQuick 2.1
import Adapter 1.0

Rectangle
{
    property string receiverName: adapter.name
    property int status: adapter.status
    color: "transparent"
    x: 24
    Connections {
        target: adapter
        function onStatusChanged()
        {
            if (adapter.status === adapter.IsOn)
            {
                return allDevices.update();
            }

            if (adapter.status === adapter.IsOff)
            {
                return allDevices.clearDevices();
            }
        }
    }

    Header
    {
        id: myNetworksHeader
        level: 1
        x: 16
        y: 32
        text: qsTr("Мои сети")
        width: 91
    }

    Section
    {
        id: bluetoothSwitchSection
        y: 76

        // Пиктограмма bluetooth:
        Image
        {
            id: bluetoothPictBackground
            x: 16
            y: 24
            width: 28
            height: width
            source: "images/bluetooth_white_28.png"
        }

        // Лэйбл bluetooth:
        Header
        {
            id: bluetoothLabel
            level: 2
            x: 56
            y: 24
            text: qsTr("Bluetooth")
        }

        // Комментарий к имени компьютера:
        Text
        {
            id: bluetoothComment
            x: 56
            y: 52
            text: qsTr("Этот компьютер будет отображаться "
                       + receiverName +
                       " в настройках Bluetooth")
            color: "#FFFFFF"
            opacity: 0.4
            font.family: mtsSansRegular.name
            font.pixelSize: 14
            font.weight: Font.Normal
        }

        // Переключатель buetooth:
        Toggle
        {
            id: bluetoothMainToggle
            x: 1050
            y: 24
            onSwitchOn:
            {
                adapter.switchOn();
                mainController.updateNearestSectionPosition()
            }
            onSwitchOff:
            {
                adapter.switchOff();
            }
            isOn: adapter.status === Adapter.IsOn ||
                  adapter.status === Adapter.IsSwitchingOff

            enabled: adapter.status === Adapter.IsOn ||
                     adapter.status === Adapter.IsOff

        }
    }

    Spinner
    {
        id: spinner
        x: myNetworksHeader.x + 1060
        y: myNetworksHeader.y + 4
        visible: adapter.status === Adapter.IsSwitchingOn ||
                 adapter.status === Adapter.IsSwitchingOff
    }

}
