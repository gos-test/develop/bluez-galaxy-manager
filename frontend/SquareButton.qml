import QtQuick 2.15

Rectangle
{
    property string buttonText: "Text"
    property alias mouseArea: clickMouseArea
    property bool highlighted: false

    readonly property string highlightedBackgroundColor: "#6734D4"
    readonly property string highlightedForegroundColor: mainTextColor
    readonly property string highlightedShadowColor: "#794CD9"

    readonly property string normalBackgroundColor: squareButtonBackgroundColor
    readonly property string normalForegroundColor: mainTextColor
    readonly property string normalShadowColor: "#5D5E61"

    id: root
    height: 24
    color: "transparent"

    Rectangle
    {
       id: shadow
       x: background.x
       y: background.y - 2
       width: root.width
       height: root.height - 2
       color: highlighted ?
                  highlightedShadowColor :
                  normalShadowColor
       radius: background.radius
    }

    Rectangle
    {
        id: background
        x: 0
        y: 2
        width: root.width
        height: root.height - 2
        radius: 6
        color: highlighted ?
                   highlightedBackgroundColor :
                   normalBackgroundColor

        Text
        {
            id: textLabel
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: highlighted ?
                       highlightedForegroundColor :
                       normalForegroundColor
            text: buttonText
            font.family: mtsSansCompact.name
            font.pixelSize: 12
            font.weight: Font.Normal

            MouseArea
            {
                id: clickMouseArea
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
            }
        }
    }
}
