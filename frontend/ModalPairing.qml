import QtQuick 2.0

Rectangle
{
    property string deviceName: "OnePlus G50"
    property string pinCode: "12345678"

    id: root
    width: 442
    height: 186
    radius: 16
    border.color:"#262626"
    color: mainBackgroundColor
    clip: true
    x: parent.width / 2 - width / 2
    y: parent.height / 2 - height / 2

    Image
    {
        id: iconPict
        source: "images/bluetooth_purple.png"
        x: 20
        y: 20
        width: 52
        height: width
    }

    Header
    {
        id: pairingTitleText
        level: 3
        text: qsTr("Требуется подтверждение подключаемого устройства")
        x: iconPict.x + iconPict.width + 16
        y: 20
    }

    Text
    {
        id: pairingText
        text: qsTr("Введите PIN - код на устройстве " + deviceName +
                   ", затем при необходимости нажмите «Ввод» на устройстве")
        color: mainTextColor
        width: 338
        wrapMode: Text.WordWrap
        x: iconPict.x + iconPict.width + 16
        y: 44
        font.family: mtsSansCompact.name
        font.pixelSize: 12
        font.weight: Font.Normal
    }

    Text
    {
        id: pinCodeText
        text: qsTr(pinCode)
        color: mainTextColor
        width: 338
        wrapMode: Text.WrapAnywhere
        x: iconPict.x + iconPict.width + 16
        y: 96
        font.family: mtsSansCompactMedium.name
        font.pixelSize: 26
        font.weight: Font.Medium
    }

    SquareButton
    {
        id: cancelButton
        width: 93
        x: root.width - width - 16
        y: root.height - height - 16
        buttonText: qsTr("Отменить")
        mouseArea.onClicked:
        {
            allDevices.discardPairing();
        }
        focus: true;
    }

}
