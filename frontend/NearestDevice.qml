import QtQuick 2.15
import Device 1.0

Item
{
    property string deviceAlias
    property string deviceClass
    property string deviceIcon
    property bool isPairing
    property bool isDebug: false
    property int deviceID: 0
    signal pair()

    id: root
    height: 44
    width: 1078

    Rectangle
    {
        x: 0
        y: 0
        id: debugRect
        width: parent.width
        height: parent.height
        color: "transparent"
    }

    Rectangle
    {
        id: devicePict
        x: 3
        y: 7
        width: 28
        height: 28
        radius: 6
        color: "transparent"
        Image
        {
            id: deviceImg
            anchors.fill: parent
            sourceSize.width: parent.width
            sourceSize.height: parent.height
            source:
            {
                if (darkMode)
                    return "images/dark/devices/" + deviceIcon + ".svg"
            }
        }
    }

    Text
    {
        id: deviceNameText
        x: 40
        y: 10
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        color: "#FAFAFA"
        text: qsTr(deviceAlias)
        font.family: mtsSansMedium.name
        font.pixelSize: 16
        font.weight: Font.Medium
    }

    Text
    {
        id: devClassDebug
        x: 300
        y: 10
        text: root.deviceClass
        height: 50
        color: "green"
        visible: isDebug
    }

    Text
    {
        id: devIconDebug
        x: 650
        y: 10
        text: root.deviceIcon
        height: 50
        color: "green"
        visible: isDebug
    }

    SquareButton
    {
        id: pairButton
        x: 980
        y: 10
        width: 98
        visible: hoverHandler.hovered
        buttonText: qsTr("Подключить")
        mouseArea.onClicked:
        {
            pair();
        }
    }

    Spinner
    {
        id: spinner
        x: pairButton.x + pairButton.width - width
        y: deviceNameText.y
        visible: isPairing;
    }

    MouseArea
    {
        id: mouseArea
        anchors.fill: parent
        z: -1
        hoverEnabled: true
        onEntered: pairButton.visible = true
        onExited: pairButton.visible = false
    }

    HoverHandler
    {
        id: hoverHandler
        enabled: !allDevices.isPairing &&
                 !myDevices.isPairing &&
                 parent.enabled
    }
}
