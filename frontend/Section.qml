import QtQuick 2.0

Rectangle
{
    property string borderColor: "#E0E0E0"
    property real borderOpacity: 0.2
    property int borderRadius: 16

    width: 1114
    height: 96
    color: "transparent"

    // Рамка:
    Rectangle
    {
        id: mainFrame
        border.color: borderColor
        color: "transparent"
        radius: borderRadius
        opacity: borderOpacity
        anchors.fill: parent
    }

}
