import QtQuick 2.0

Rectangle
{
    property string deviceName
    property int deviceID: -1

    id: root
    width: 437
    height: 152
    radius: 16
    visible: false
    border.color:"#262626"
    color: mainBackgroundColor
    clip: true
    x: parent.width / 2 - width / 2
    y: parent.height / 2 - height / 2
    

    Image
    {
        id: iconPict
        source: "images/bluetooth_purple.png"
        x: 20
        y: 20
        width: 52
        height: width
        fillMode: Image.PreserveAspectFit
    }

    Header
    {
        id: pairingTitleText
        level: 3
        text: qsTr("Вы уверены что хотите забыть устройство
\"" + deviceName +"\"?")
        x: iconPict.x + iconPict.width + 16
        y: 20
        wrapMode: Text.WordWrap
    }

    Text
    {
        id: pairingText
        text: qsTr("Это устройство не подключится снова автоматически.
Потом вам придется подключать его заново.")
        color: mainTextColor
        width: 338
        wrapMode: Text.WordWrap
        x: iconPict.x + iconPict.width + 16
        y: 60
        font.family: mtsSansCompact.name
        font.pixelSize: 12
        font.weight: Font.Normal
    }

    SquareButton
    {
        id: cancelButton
        width: 85
        x: 185
        y: 108
        buttonText: qsTr("Отменить")
        mouseArea.onClicked:
        {
            root.visible = false
        }
        focus: true;
    }

    SquareButton
    {
        id: forgetButton
        width: 135
        x: 282
        y: 108
        buttonText: qsTr("Забыть устройство")
        mouseArea.onClicked:
        {
            allDevices.removeDevice();
            root.visible = false;
        }
        focus: true;
        highlighted: true
    }

}
