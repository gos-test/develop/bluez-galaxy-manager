import QtQuick 2.0
import QtQuick.Window 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.15
import Adapter 1.0
import Device 1.0

Window
{
    id: root
    property string mainBackgroundColor: "#191A1E"
    property string mainTextColor: "#FAFAFA"
    property string squareButtonBackgroundColor: "#47484b"
    property bool darkMode: true
    width: 1400
    height: 900
    visible: true
    color: mainBackgroundColor
    title: qsTr("BlueZ Manager")

    Item
    {
        id: mainController

        function updateNearestSectionPosition()
        {
            var count = myDevices.rowCount();
            nearestDevicesSection.y = count > 0 ?
                        count * 65  + 350 :
                        409
        }

        function showModalInfoWindow(deviceID)
        {
            allDevices.selectDevice(deviceID);
            modalInfo.deviceID = deviceID;
            modalInfo.deviceAlias = allDevices.currentDeviceAlias();
            modalInfo.deviceSN = allDevices.currentDeviceSerialNumber();
            modalInfo.aliasIsEditable = allDevices.isCurrentDeviceConnected();
            modalInfo.deviceModelName = allDevices.currentDeviceName();
            modalInfo.deviceModel = allDevices.currentDeviceModel();
            modalInfo.deviceVersion = allDevices.currentDeviceVersion();
            modalInfo.visible = true;
        }

        function showModalConfirmation(deviceID)
        {
            modalConfiramtion.deviceID = (deviceID);
            modalConfiramtion.visible = true;
        }
    }

    // ШРИФТЫ (начало) --------------------------------------------------------
    FontLoader
    {
        id: mtsSansMedium
        source: "fonts/MTS_21_v2_002_TTF/Text/MTSText-Medium.ttf"
    }

    FontLoader
    {
        id: mtsSansRegular
        source: "fonts/MTS_21_v2_002_TTF/Text/MTSText-Regular.ttf"
    }

    FontLoader
    {
        id: mtsSansCompact
        source: "fonts/MTS_21_v2_002_TTF/Compact/MTSCompact-Regular.ttf"
    }

    FontLoader
    {
        id: mtsSansCompactMedium
        source: "fonts/MTS_21_v2_002_TTF/Compact/MTSCompact-Medium.ttf"
    }

    FontLoader
    {
        id: mtsSansCompactBold
        source: "fonts/MTS_21_v2_002_TTF/Compact/MTSCompact-Bold.ttf"
    }
    // ШРИФТЫ (конец) --------------------------------------------------------

    Column
    {
        id: columnLayout
        spacing: 2
        anchors.horizontalCenter: parent.horizontalCenter
        width: 1114

        // ВКЛЮЧЕНИЕ BLUETOOTH (начало) -------------------------------------------
        AdapterSection
        {
            id: adapterSection
            Layout.alignment: Qt.AlignTop && Qt.AlignCenter
            Layout.preferredWidth: 1114
            Layout.preferredHeight: 230
            enabled: !modalPairingWindow.visible &&
                     !modalConfiramtion.visible &&
                     !modalInfo.visible
        }
        // ВКЛЮЧЕНИЕ BLUETOOTH (конец) --------------------------------------------

        // СПИСОК УСТРОЙСТВ (начало) ----------------------------------------------
        MyDevicesSection
        {
            id: myDevicesSection
            Layout.alignment: Qt.AlignTop && Qt.AlignCenter
            Layout.preferredWidth: 1114
            Layout.preferredHeight: height
            height: myDevices.rowCount() > 0 ?
                                    myDevices.rowCount() * 65  + 205 :
                                    175
            enabled: !modalPairingWindow.visible &&
                     !modalConfiramtion.visible &&
                     !modalInfo.visible
            onVisibleChanged:
            {
                mainController.updateNearestSectionPosition()
            }
        }
        // СПИСОК УСТРОЙСТВ (конец) -------------------------------------------

        // БЛИЖАЙШИЕ УСТРОЙСТВА (начало) --------------------------------------
        NearestDevicesSection
        {
            id: nearestDevicesSection
            y: myDevices.rowCount() > 0 ? myDevices.rowCount() * 65  + 364 :  435
            Layout.alignment: Qt.AlignTop && Qt.AlignCenter
            Layout.preferredWidth: 1114
            Layout.preferredHeight: 150
            enabled: !modalPairingWindow.visible &&
                     !modalConfiramtion.visible &&
                     !modalInfo.visible
            onVisibleChanged:
            {
                if (visible)
                    mainController.updateNearestSectionPosition()
            }
        }
        // БЛИЖАЙШИЕ УСТРОЙСТВА (конец) ---------------------------------------
    }

    // МОДАЛЬНОЕ ОКНО ПОДКЛЮЧЕНИЯ (начало) ------------------------------------
    ModalPairing
    {
        id: modalPairingWindow
        visible: allDevices.isPairing
        onVisibleChanged:
        {
            if (visible)
            {
                deviceName = allDevices.currentDeviceAlias()
            }
        }
    }
    // МОДАЛЬНОЕ ОКНО СООБЩЕНИЯ (конец) ---------------------------------------

    // МОДАЛЬНОЕ ОКНО ИНФОРМАЦИИ (начало) -------------------------------------
    ModalInfo
    {
        id: modalInfo
        visible: false
        onDoneClicked:
        {
            allDevices.setDeviceAlias(modalInfo.deviceAlias);
            visible = false;
        }
        onDisconnectClicked:
        {
            if (allDevices.isCurrentDeviceConnected())
            {
                allDevices.disconnect(modalInfo.deviceID);
                disconnectButtonText = qsTr("Подключить");
            }
            else
            {
                allDevices.connect(modalInfo.deviceID);
                disconnectButtonText = qsTr("Отключить");
            }
            visible = false;
        }
        onForgetClicked:
        {
            visible = false;
            mainController.showModalConfirmation(deviceID);
        }
        onVisibleChanged:
        {
            if (visible)
            {
                if (allDevices.isCurrentDeviceConnected())
                    disconnectButtonText = qsTr("Отключить");
                else
                    disconnectButtonText = qsTr("Подключить");
            }
        }
    }
    // МОДАЛЬНОЕ ОКНО ИНФОРМАЦИИ (конец) --------------------------------------

    // МОДАЛЬНОЕ ОКНО ПОДТВЕРЖДЕНИЯ (начало) ----------------------------------
    ModalConfirmation
    {
        id: modalConfiramtion
        onVisibleChanged:
        {
            if (visible)
            {
                deviceName = allDevices.currentDeviceAlias();       
            }
        }

    }
    // МОДАЛЬНОЕ ОКНО ИНФОРМАЦИИ (конец) --------------------------------------

}
