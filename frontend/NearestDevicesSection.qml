import QtQuick 2.0
import Adapter 1.0

Rectangle {

    id: nearestDevicesGroup
    property bool isProcessing: false
    color: "transparent"
    enabled:
    {
       return !myDevices.isPairing && !myDevices.isBusy && !allDevices.isConnecting && !allDevices.isBusy
    }
    visible: adapter.status === Adapter.IsOn ||
             adapter.status === Adapter.IsSwitchingOff
    x: 24

    Connections
    {
        target: allDevices
        function onIsBusyChanged()
        {
            isProcessing = allDevices.isBusy;
            console.log(isProcessing);
        }
    }

    Header
    {
        id: nearestDevicesHeader
        level: 1
        x: 16
        text: qsTr("Ближайшие устройства")
    }

    NearestDevicesList
    {
        id: nearestDevicesList
        y: 44
    }

    Spinner
    {
        id: nearestDevicesSpinner
        x: nearestDevicesHeader.x + 1060
        y: nearestDevicesHeader.y + 4
        visible: allDevices.isPairing
    }

}
