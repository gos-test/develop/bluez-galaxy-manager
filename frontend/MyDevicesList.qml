import QtQuick 2.0
import Device 1.0

Rectangle
{
    property bool isDetailed: false
    readonly property int count: devicesList.count
    Section
    {
        height: devicesList.count > 0 ? devicesList.count * 65  + 24 : 72

        ListView
        {
            id: devicesList
            anchors.fill: parent
            anchors.topMargin: 15
            anchors.bottomMargin: 15
            anchors.leftMargin: 15
            anchors.rightMargin: 15
            height: parent.height
            model: myDevices
            delegate:
            MyDevice
            {
                deviceAlias: model.deviceAlias
                status: model.deviceStatus
                chargeLevel: model.batteryLevel
                isProcessing: model.deviceStatus === Device.Connecting ||
                              model.deviceStatus === Device.Disconnecting ||
                              model.deviceStatus === Device.Pairing ||
                              model.deviceStatus === Device.Unpairing
                onClicked:
                {
                    if (model.deviceStatus === Device.Paired)
                        return allDevices.connect(model.id);

                    if (model.deviceStatus === Device.Connected)
                        return allDevices.disconnect(model.id);
                }

                onInfoClicked:
                {
                    allDevices.selectDevice(model.id);
                    mainController.showModalInfoWindow(model.id);
                }
                icon: model.icon

            }
            onCountChanged: mainController.updateNearestSectionPosition()
        }

        Text
        {
            id: noDevicesStatus
            width: parent.width
            height: parent.height
            visible: devicesList.count === 0
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Устройства отсутствуют")
            color: "#969FA8"
            font.family: mtsSansMedium.name
            font.pixelSize: 16
            font.weight: Font.Medium
        }
    }
}

