import QtQuick 2.1

Rectangle
{
    property int deviceID
    property string deviceAlias
    property bool aliasIsEditable
    property string deviceModelName
    property string deviceSN: "XXXXXXXXXX"
    property string deviceVersion: "XXXXXXXXXX"
    property string deviceModel: "XXXXXXXXXX"
    property string disconnectButtonText: qsTr("Подключить")

    readonly property string nameLabelText: qsTr("Имя")
    readonly property string modelNameLabelText: qsTr("Название модели")
    readonly property string snLabelText: qsTr("Серийный номер")
    readonly property string versionLabelText: qsTr("Версия")
    readonly property string modelNumberLabelText: qsTr("Номер модели")

    readonly property string forgetButtonText: qsTr("Забыть это устройство")

    readonly property string connectButtonText: qsTr("Подключить")
    readonly property string doneButtonText: qsTr("Готово")


    signal forgetClicked()
    signal disconnectClicked()
    signal doneClicked()


    id: root
    width: 449
    height: 338
    radius: 16
    border.color:"#262626"
    color: mainBackgroundColor
    clip: true
    x: parent.width / 2 - width / 2
    y: parent.height / 2 - height / 2

    Section
    {
        id: nameSection
        x: 20
        y: 20
        width: 409
        height: 48

        Text
        {
            id: nameLabel
            text: nameLabelText
            font.family: mtsSansCompactMedium.name
            font.pixelSize: 12
            font.weight: Font.Medium
            x: 12
            y: 16
            color: mainTextColor
        }

        TextEdit
        {
            id: deviceAliasInput
            x: 210
            y: 14
            width: 187
            height: 24
            font.family: mtsSansCompact.name
            font.pixelSize: 14
            horizontalAlignment: "AlignRight"
            text: deviceAlias
            color: mainTextColor
            wrapMode: "NoWrap"
            clip:true
            selectByKeyboard: true
            selectByMouse: true
            enabled: aliasIsEditable
        }

    }

    Section
    {
        id: infoSection
        x: 20
        y: nameSection.y + nameSection.height + 12
        width: 409
        height: 190

        // Название модели (НАЧАЛО) -------------------------------------------
        Text
        {
            id: modelNameLabel
            text: modelNameLabelText
            font.family: mtsSansCompactMedium.name
            font.pixelSize: 12
            font.weight: Font.Medium
            x: 12
            y: 24
            color: mainTextColor
            verticalAlignment: Text.AlignVCenter
        }

        Text
        {
            id: modelNameValue
            text: deviceModelName
            font.family: mtsSansCompactMedium.name
            font.pixelSize: 12
            font.weight: Font.Medium
            x: infoSection.width - width - 12
            y: modelNameLabel.y + modelNameLabel.height / 2 - height /2
            color: mainTextColor
            width: 187
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
            clip: true
        }
        // Название модели (КОНЕЦ) --------------------------------------------

        // Серийный номер (НАЧАЛО) --------------------------------------------
        Text
        {
            id: snLabel
            text: snLabelText
            font.family: mtsSansCompactMedium.name
            font.pixelSize: 12
            font.weight: Font.Medium
            x: 12
            y: modelNameLabel.y + modelNameLabel.height + 26
            color: mainTextColor
            verticalAlignment: Text.AlignVCenter
        }

        Text
        {
            id: snLabelValue
            text: deviceSN
            font.family: mtsSansCompactMedium.name
            font.pixelSize: 12
            font.weight: Font.Medium
            x: infoSection.width - width - 12
            y: snLabel.y + snLabel.height / 2 - height /2
            color: mainTextColor
            width: 187
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
            clip: true
        }
        // Серийный номер (КОНЕЦ) ---------------------------------------------

        // Версия (НАЧАЛО) ----------------------------------------------------
        Text
        {
            id: versionLabel
            text: versionLabelText
            font.family: mtsSansCompactMedium.name
            font.pixelSize: 12
            font.weight: Font.Medium
            x: 12
            y: snLabel.y + snLabel.height + 26
            color: mainTextColor
            verticalAlignment: Text.AlignVCenter
        }

        Text
        {
            id: versionLabelValue
            text: deviceVersion
            font.family: mtsSansCompactMedium.name
            font.pixelSize: 12
            font.weight: Font.Medium
            x: infoSection.width - width - 12
            y: versionLabel.y + versionLabel.height / 2 - height /2
            color: mainTextColor
            width: 187
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
            clip: true
        }
        // Версия (КОНЕЦ) -----------------------------------------------------

        // Номер модели (НАЧАЛО) ----------------------------------------------
        Text
        {
            id: modelNumberLabel
            text: modelNumberLabelText
            font.family: mtsSansCompactMedium.name
            font.pixelSize: 12
            font.weight: Font.Medium
            x: 12
            y: versionLabel.y + versionLabel.height + 26
            color: mainTextColor
            verticalAlignment: Text.AlignVCenter
        }

        Text
        {
            id: modelNumberLabelValue
            text: deviceModel
            font.family: mtsSansCompactMedium.name
            font.pixelSize: 12
            font.weight: Font.Medium
            x: infoSection.width - width - 12
            y: modelNumberLabel.y + modelNumberLabel.height / 2 - height /2
            color: mainTextColor
            width: 187
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
            clip: true
        }
        // Номер модели (КОНЕЦ) -----------------------------------------------

    }

    SquareButton
    {
        id: forgetButton
        x: 20
        y: infoSection.y + infoSection.height + 24
        width: 155
        height: 24
        buttonText: forgetButtonText
        mouseArea.onClicked:
        {
            forgetClicked()
        }
    }

    SquareButton
    {
        id: disconnectButton
        x: forgetButton.x + forgetButton.width + 12
        y: infoSection.y + infoSection.height + 24
        //visible: isDeviceConnected
        width: 91
        height: 24
        buttonText: disconnectButtonText
        mouseArea.onClicked: disconnectClicked()
    }

    SquareButton
    {
        id: doneButton
        x: infoSection.x + infoSection.width - width
        y: infoSection.y + infoSection.height + 24
        width: 67
        height: 24
        buttonText: doneButtonText
        mouseArea.onClicked:
        {
            deviceAlias = deviceAliasInput.text
            doneClicked();
        }
        highlighted: true
    }

}
