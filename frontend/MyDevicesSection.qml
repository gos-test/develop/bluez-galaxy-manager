import QtQuick 2.0
import Adapter 1.0

Rectangle
{
    id: myDevicesGroup
    enabled:
    {
       return !myDevices.isPairing && !myDevices.isBusy &&
              !allDevices.isConnecting && !allDevices.isBusy
    }
    visible: adapter.status === Adapter.IsOn ||
             adapter.status === Adapter.IsSwitchingOff
    y: 230
    x: 24
    color: "transparent"

    Header
    {
        id: myDevicesHeader
        level: 1
        x: 16
        text: qsTr("Мои устройства")
    }

    MyDevicesList
    {
        id: myDevicesList
        y: 44
        isDetailed: true
    }

    Spinner
    {
        id: myDevicesSpinner
        x: myDevicesHeader.x + 1060
        y: myDevicesHeader.y + 4
        visible: allDevices.isConnecting || allDevices.isDisconnecting
    }
}
