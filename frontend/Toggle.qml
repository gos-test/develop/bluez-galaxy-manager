import QtQuick 2.5

Item
{
    id: root
    property bool isOn: true
    property int animationDuration: 100
    property string colorWhenOn: "#6734D4"
    property string colorWhenOff: "#47484b"
    property string capColor: "#E7EAFA"
    property int leftCapMargin: 2
    property int topCapMargin: 2

    signal switchOn()
    signal switchOff()

    width: 44
    height: 24

    Rectangle
    {
        id: toggleBackground
        radius: 14
        color: root.isOn ? root.colorWhenOn : root.colorWhenOff
        width: parent.width
        height: parent.height
        Behavior on color
        {ColorAnimation{duration: root.animationDuration}}
    }

    Rectangle
    {
        id: toggleCap
        radius: width / 2
        width: height
        height: root.height - 2 * root.topCapMargin
        x: root.isOn ?
               root.width - width - root.leftCapMargin :
               root.leftCapMargin
        y: root.topCapMargin
        color: root.capColor
        Behavior on x
        {PropertyAnimation{duration: root.animationDuration}}
    }

//    HoverHandler
//    {
//        id: hoverHandler
//    }

    MouseArea
    {
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        onClicked:
        {
            if (isOn) return switchOff();
            if (!isOn) return switchOn();
        }
    }
}
