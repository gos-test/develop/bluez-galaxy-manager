import QtQuick 2.0

Item
{
    id: root
    width: 22
    height: width

    Image {
        id: image
        anchors.fill: parent
        sourceSize.width: width
        sourceSize.height: height
        fillMode: Image.PreserveAspectFit
        source: "images/spinner.png"
    }

    RotationAnimation on rotation
    {
        id: rotationAnimation
        from: 0
        to: -360
        duration: 1500
        running: root.visible
        loops: Animation.Infinite
        //direction: RotationAnimation.Counterclockwise
    }
}
