import QtQuick 2.15
import Device 1.0

Item
{
    property string deviceAlias
    property int status
    property int chargeLevel
    property string icon
    property bool isProcessing: false
    readonly property string greenStatusColor: "#1DD15A"
    readonly property string yellowStatusColor: "#FFBD5A"
    readonly property string redStatusColor: "#FF5A5A"
    readonly property string pairedStatusText: qsTr("Отключено")
    readonly property string connectedStatusText: qsTr("Подключено")
    readonly property string disconnectingStatusText: qsTr("Отключение")
    readonly property string connectingStatusText: qsTr("Подключение")
    readonly property string connectButtonText: qsTr("Подключить")
    readonly property string disconnectButtonText: qsTr("Отключить")
    signal clicked()
    signal infoClicked()

    id: root
    height: 65
    width: 1078

    function disconnect(id)
    {
        console.log(LoggingCategory.Info, "disconnect(" + id + ")");
        return 0;
    }

    Rectangle
    {
        id: devicePict
        y: 8
        width: 28
        height: 28
        radius: 6
        color: root.status === Device.Connected ? "#7543df" : "#4b4b4b"
        Image
        {
            id: deviceImg
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            width: 26
            height: 26
            sourceSize.width: width
            sourceSize.height: height
            visible: root.status === Device.Paired ||  root.status === Device.Connected
            source:
            {
                var mode = darkMode ? "dark" : "light"
                return "images/" + mode + "/devices/" + icon + ".svg"
            }
        }
    }

    Text
    {
        id: deviceNameText
        x: 40
        y: 10
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        color: "#FAFAFA"
        text: qsTr(deviceAlias)
        font.family: mtsSansMedium.name
        font.pixelSize: 16
        font.weight: Font.Medium
    }

    SquareButton
    {
        id: disconnectButton
        x: deviceNameText.x + 917
        y: 10
        width: 98
        visible: hoverHandler.hovered &&
                 (root.status === Device.Paired || root.status === Device.Connected)
        buttonText:
        {
            if (root.status === Device.Paired)
                return root.connectButtonText;
            else
                return root.disconnectButtonText;
        }

        mouseArea.onClicked: root.clicked();
    }

    Image
    {
        id: infoIcon
        x: disconnectButton.x + disconnectButton.width + 12
        y: disconnectButton.y + disconnectButton.height / 2 - height / 2
        source: "images/info_icon.png"

        MouseArea
        {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            onClicked: infoClicked()
        }
    }

    Rectangle
    {
        id: statusIndicator
        x: 44
        y: 41
        height: 8
        width: height
        radius: 4
        color:
        {
            if (root.status === Device.Paired) return redStatusColor;
            if (root.status === Device.Connecting) return yellowStatusColor;
            if (root.status === Device.Disconnecting) return yellowStatusColor;
            if (root.status === Device.Connected) return greenStatusColor;

            return redStatusColor;
        }
    }

    Text
    {
        id: deviceStatusText
        x: 58
        y: 35
        height: 18
        verticalAlignment: Text.AlignVCenter
        color: "#757678"
        text:
        {
            if (status === Device.Paired) return pairedStatusText
            if (status === Device.Connecting) return connectingStatusText
            if (status === Device.Disconnecting) return disconnectingStatusText
            if (status === Device.Connected) return connectedStatusText

            return disconnectingStatusText;
        }
        font.family: mtsSansRegular.name
        font.pixelSize: 14
        font.weight: Font.Normal
    }

    Item
    {
        id: batteryInfo
        visible: status === Device.Connected

        Image {
            id: batteryPict
            x: deviceStatusText.x + deviceStatusText.width + 6
            y: 34
            width: 20
            sourceSize.width: width
            fillMode: Image.PreserveAspectFit
            source: "images/battery.svg"
        }

        Rectangle
        {
            id: batteryChargePict
            x: batteryPict.x + 3
            y: batteryPict.y + 8
            width: chargeLevel * (batteryPict.width - 8) / 100
            height: batteryPict.height - 16
            color: "#A3A3A5"
        }

        Text
        {
            id: batteryChargeLevel
            x: 170
            y: 36
            height: deviceStatusText.height
            verticalAlignment: Text.AlignVCenter
            color: "#757678"
            text: chargeLevel + " %"
            font.family: mtsSansRegular.name
            font.pixelSize: 14
            font.weight: Font.Normal
        }
    }

    Spinner
    {
        id: myDevicesSpinner
        x: devicePict.x + devicePict.width / 2 - width / 2
        y: devicePict.y + devicePict.height / 2 - height / 2
        visible: root.isProcessing
        width: 16
    }
    MouseArea
    {
        anchors.fill: parent
        z: -1
    }

    HoverHandler
    {
        id: hoverHandler
        enabled: !allDevices.isPairing && !myDevices.isPairing && parent.enabled
    }
}
