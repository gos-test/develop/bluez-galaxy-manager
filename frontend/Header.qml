import QtQuick 2.0

Item {
    id: root
    property int level: 1
    property alias text: headerText.text
    property alias wrapMode: headerText.wrapMode

    readonly property string level_1_color: "#FAFAFA"
    readonly property string level_2_color: "#FAFAFA"
    readonly property string level_3_color: "#FAFAFA"


    readonly property int level_1_fontSize: 20
    readonly property int level_2_fontSize: 16
    readonly property int level_3_fontSize: 12

    Text
    {
        id: headerText
        color:
        {
            if (root.level == 1) return level_1_color
            if (root.level == 2) return level_2_color
            if (root.level == 3) return level_3_color
        }
        font.family:
        {
            if (root.level == 1) return mtsSansMedium.name
            if (root.level == 2) return mtsSansMedium.name
            if (root.level == 3) return mtsSansCompactBold.name
        }
        font.pixelSize:
        {
            if (root.level == 1) return level_1_fontSize
            if (root.level == 2) return level_2_fontSize
            if (root.level == 3) return level_3_fontSize
        }
        font.weight:
        {
            if (root.level == 1) return Font.Medium
            if (root.level == 2) return Font.Medium
            if (root.level == 3) return Font.Bold
        }
        wrapMode: Text.WordWrap
    }
}
