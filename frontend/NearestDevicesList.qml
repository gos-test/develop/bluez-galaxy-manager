import QtQuick 2.15
import Device 1.0

Rectangle
{
    readonly property int count: devicesList.count
    property int currentID: 0
    readonly property int delegateHeight: 44
    property int maxSectionHeight: 400

    Section
    {
        id: section
        height:
        {
           if (devicesList.count === 0)
                return 72
           else
                return  devicesList.topMargin +
                        devicesList.bottomMargin +
                        delegateHeight * (devicesList.count) +
                        devicesList.spacing * (devicesList.count + 1);
        }

        ListView
        {
            id: devicesList
            anchors.fill: parent
            anchors.topMargin: 16
            anchors.bottomMargin: 16
            anchors.leftMargin: 16
            anchors.rightMargin: 16
            height: parent.height
            interactive: false
            model: nearestDevices
            spacing: 12
            delegate:
            NearestDevice
            {
                deviceAlias: model.deviceAlias
                deviceClass: model.deviceClass
                deviceIcon: model.icon
                isPairing: model.deviceStatus === Device.Pairing
                deviceID: model.id
                onPair:
                {
                    console.log(model.id);
                    allDevices.pair(model.id)
                }
            }
        }

        Text
        {
            id: noDevicesStatus
            width: parent.width
            height: parent.height
            visible: devicesList.count === 0
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Устройства отсутствуют")
            color: "#969FA8"
            font.family: mtsSansMedium.name
            font.pixelSize: 16
            font.weight: Font.Medium
        }
    }
}

