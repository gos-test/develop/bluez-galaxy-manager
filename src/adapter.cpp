#include "adapter.h"
#include "dbusservice.h"
#include "devices.h"
#include "settings.h"
#include <QTimer>

void Adapter::declareQML()
{
    qmlRegisterType<Adapter>("Adapter", 1, 0, "Adapter");
}

Adapter::~Adapter()
{}

void Adapter::switchOn()
{
    setStatus(IsSwitchingOn);
    if (!Settings::isDemo)
        DBusService::getInstance()->setPowered(true);
    else
        QTimer::singleShot(Settings::demoSimulationPeriod,
                           this, &Adapter::onSwitchOn);
}

void Adapter::switchOff()
{
    setStatus(IsSwitchingOff);
    if (!Settings::isDemo)
        DBusService::getInstance()->setPowered(false);
    else
        QTimer::singleShot(Settings::demoSimulationPeriod,
                           this, &Adapter::onSwitchOff);
}

Adapter::Adapter(QObject *parent)
    : QObject{parent}
{
    setName(DBusService::getInstance()->getName());
    DBusService::getInstance()->getPowered();
    setStatus(IsOff);
    connect (DBusService::getInstance(), SIGNAL(poweredChanged(bool)),
             this, SLOT(onPoweredChanged(bool)));

}

Adapter::AdapterStatus Adapter::status() const
{
    return m_status;
}

void Adapter::setStatus(AdapterStatus newStatus)
{
    m_status = newStatus;
    emit statusChanged();
}

QString Adapter::name() const
{
    return m_name;
}

void Adapter::setName(const QString &newName)
{
    m_name = newName;
    emit receiverNameChanged();
}

void Adapter::onSwitchOn()
{
    setStatus(IsOn);
    if (!Settings::isDemo)
        DBusService::getInstance()->startDiscovery();

    Devices::getInstance()->update();
}

void Adapter::onSwitchOff()
{
    Devices::getInstance()->disconnectAllDevices(); // for demo
    setStatus(IsOff);
}

void Adapter::onPoweredChanged(bool powered)
{
    if (powered)
        onSwitchOn();
    else
        onSwitchOff();
}
