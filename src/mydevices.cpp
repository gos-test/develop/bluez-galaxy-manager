#include "mydevices.h"
#include <QDebug>
#include <QTimer>
#include "device.h"
#include "devices.h"


MyDevicesModel* MyDevicesModel::instance = nullptr;

MyDevicesModel::MyDevicesModel(QObject *parent):
    m_currentDevice(nullptr)
{
}

MyDevicesModel *MyDevicesModel::getInstance()
{
    if (!instance)
        instance = new MyDevicesModel();

    return instance;
}

MyDevicesModel::~MyDevicesModel()
{
    instance->deleteLater();
    instance = nullptr;
}

QHash<int, QByteArray> MyDevicesModel::roleNames() const
{
    QHash<int, QByteArray> roleNames;
    roleNames[IdRole] = QByteArrayLiteral("id");
    roleNames[NameRole] = QByteArrayLiteral("deviceName");
    roleNames[AliasRole] = QByteArrayLiteral("deviceAlias");
    roleNames[StatusRole] = QByteArrayLiteral("deviceStatus");
    roleNames[BatteryLevelRole] = QByteArrayLiteral("batteryLevel");
    return roleNames;
}

void MyDevicesModel::connectDevice(int id)
{
    qDebug() << "connectDevice" << id;

    for (auto device : devices)
    {
        if (device->deviceID() == id)
        {
            beginResetModel();
            setCurrentDevice(device);
            setIsPairing(true);
            currentDevice()->setDeviceStatus(Device::Connecting);
            QModelIndex start = this->index(0, 0);
            QModelIndex end = this->index(this->rowCount() - 1, 0);
            emit dataChanged(start, end);
            endResetModel();
            QTimer::singleShot(3000, this, &MyDevicesModel::onDeviceConnected);
            return;
        }
    }
    setIsPairing(false);
}

void MyDevicesModel::disconnectDevice(int id)
{
    qDebug() << "connectDevice" << id;

    for (auto device : devices)
    {
        if (device->deviceID() == id)
        {
            beginResetModel();
            setCurrentDevice(device);
            setIsPairing(true);
            if (currentDevice()->deviceStatus() == Device::Connected)
                currentDevice()->setDeviceStatus(Device::Disconnecting);

            QModelIndex start = this->index(0, 0);
            QModelIndex end = this->index(this->rowCount() - 1, 0);
            emit dataChanged(start, end);
            endResetModel();
            if (currentDevice()->deviceStatus() == Device::Disconnecting)
                QTimer::singleShot(3000, this, &MyDevicesModel::onDeviceDisconnected);
            else
                QTimer::singleShot(0, this, &MyDevicesModel::onDeviceDisconnected);
            return;
        }
    }
    setIsPairing(false);
}

QString MyDevicesModel::getDeviceName(int id)
{
    for (auto d : devices)
    {
        if (d->deviceID() == id)
        {
            return d->deviceName();
        }
    }
    return QString();
}

void MyDevicesModel::setDeviceName(int id, QString newName)
{
    for (auto d : devices)
    {
        if (d->deviceID() == id)
        {
            d->setDeviceName(newName);
            QModelIndex start = this->index(0, 0);
            QModelIndex end = this->index(this->rowCount() - 1, 0);
            emit dataChanged(start, end);
        }
    }
}

int MyDevicesModel::getDeviceStatus(int id)
{
    for (auto d : devices)
    {
        if (d->deviceID() == id)
        {
            return d->deviceStatus();
        }
    }
    return Device::DeviceStatus::Disconnected;
}

void MyDevicesModel::forgetDevice(int id)
{
    qDebug() << "Forget device:" << id;
    disconnectDevice(id);
    m_toForget = true;
}

int MyDevicesModel::getCount()
{
    return devices.size();
}

void MyDevicesModel::selectDevice(int id)
{
    for (auto d : devices)
    {
        if (d->deviceID() == id)
        {
            setCurrentDevice(d);
        }
    }
}

int MyDevicesModel::currentDeviceId()
{

    if (!m_currentDevice)
        return -1;

    qDebug() << "current ID:" << currentDevice()->deviceID();
    return currentDevice()->deviceID();
}

QString MyDevicesModel::currentDeviceName()
{
    if (!m_currentDevice)
        return QString();

    return currentDevice()->deviceName();
}

QString MyDevicesModel::currentModelName()
{
    if (!m_currentDevice)
        return QString();

    return currentDevice()->modelName();
}

QString MyDevicesModel::currentSerialNumber()
{
    if (!m_currentDevice)
        return QString();

    return currentDevice()->serialNumber();
}

QString MyDevicesModel::currentVersion()
{
    if (!m_currentDevice)
        return QString();

    return currentDevice()->version();
}

QString MyDevicesModel::currentModelNumber()
{
    if (!m_currentDevice)
        return QString();

    return currentDevice()->modelNumber();
}

int MyDevicesModel::currentDeviceStatus()
{
    if (!m_currentDevice)
        return Device::DeviceStatus::Disconnected;

    return currentDevice()->deviceStatus();
}

void MyDevicesModel::addDevice(DevicePtr device)
{
    beginResetModel();
    devices.push_back(device);
    QModelIndex start = this->index(0, 0);
    QModelIndex end = this->index(this->rowCount() - 1, 0);
    emit dataChanged(start, end);
    endResetModel();
}

DevicePtr MyDevicesModel::currentDevice() const
{
    return m_currentDevice;
}

void MyDevicesModel::setCurrentDevice(DevicePtr newDevice)
{
    m_currentDevice = newDevice;
    emit currentDeviceChanged();
}

bool MyDevicesModel::isPairing() const
{
    return m_isPairing;
}

void MyDevicesModel::setIsPairing(bool newValue)
{
    m_isPairing = newValue;
    emit isPairingChanged();
}

bool MyDevicesModel::isConnecting() const
{
    return m_isConnecting;
}

void MyDevicesModel::setIsConnecting(bool newValue)
{
    m_isConnecting = newValue;
}

int MyDevicesModel::rowCount(const QModelIndex &parent) const
{
    return devices.size();
}

QVariant MyDevicesModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() > devices.count())
        return QVariant();

    DevicePtr device = devices.at(index.row());

    if (role == IdRole)
    {
        qDebug() << device->deviceID();
        return device->deviceID();
    }

    if (role == NameRole)
    {
        qDebug() << device->deviceName();
        return device->deviceName();
    }

    if (role == StatusRole)
    {
        qDebug() << device->deviceStatus();
        return device->deviceStatus();
    }

    if (role == AliasRole)
    {
        qDebug() << device->alias();
        return device->alias();
    }

    if (role == BatteryLevelRole)
    {
        qDebug() << device->batteryLevel();
        return device->batteryLevel();
    }

    return QVariant();
}

void MyDevicesModel::onDeviceConnected()
{
    //For Demo only:

    if (!m_isPairing)
        return;

    beginResetModel();
    qDebug() << "connected";
    m_currentDevice->setDeviceStatus(Device::Connected);
    QModelIndex start = this->index(0, 0);
    QModelIndex end = this->index(this->rowCount() - 1, 0);
    emit dataChanged(start, end);
    endResetModel();
    setIsPairing(false);
}

void MyDevicesModel::onDeviceDisconnected()
{
    if (!m_isPairing)
        return;

    beginResetModel();
    qDebug() << "disconnected";
    m_currentDevice->setDeviceStatus(Device::Disconnected);
    if (m_toForget)
    {
        currentDevice()->setIsSaved(false);
        devices.removeOne(currentDevice());
        Devices::getInstance()->addDevice(currentDevice());
        m_toForget = false;
    }

    QModelIndex start = this->index(0, 0);
    QModelIndex end = this->index(this->rowCount() - 1, 0);
    emit dataChanged(start, end);
    endResetModel();
    setIsPairing(false);
}
