#include "dbusservice.h"
#include <QtDBus/QDBusMetaType>
#include <QDebug>
#include <QDomDocument>

const QString BLUEZ_SERVICE ("org.bluez");
const QString BLUEZ_PATH ("/org/bluez/hci0");
const QString  BLUEZ_ADAPTER ("org.bluez.Adapter1");
const QString  BLUEZ_DEVICE ("org.bluez.Device1");
const QString  FREEDESKTOP_PROPERTIES ("org.freedesktop.DBus.Properties");
const QString  FREEDESKTOP_INTROSPECTABLE ("org.freedesktop.DBus.Introspectable");

DBusService* DBusService::instance = nullptr;

DBusService *DBusService::getInstance(QObject* parent)
{
    if (!instance)
        instance = new DBusService(parent);

    return instance;
}

DBusService::~DBusService()
{
    delete instance;
    instance = nullptr;
}

void DBusService::startDiscovery()
{
    QDBusInterface iAdapter(BLUEZ_SERVICE, BLUEZ_PATH, BLUEZ_ADAPTER, QDBusConnection::systemBus());

    if (!iAdapter.isValid())
    {
        qCritical() << "Adapter is not valid";
    }

    QDBusReply<QVariant> r = iAdapter.call(QDBus::CallMode::AutoDetect, "StartDiscovery");

    if (!r.isValid())
    {
        qCritical() << "Reply is not valid:" << r.error();
    }
}

void DBusService::stopDiscovery()
{
    QDBusInterface iAdapter(BLUEZ_SERVICE, BLUEZ_PATH, BLUEZ_ADAPTER, QDBusConnection::systemBus());

    if (!iAdapter.isValid())
    {
        qCritical() << "Adapter is not valid";
    }

    QDBusReply<QVariant> r = iAdapter.call(QDBus::CallMode::AutoDetect, "StopDiscovery");

    if (!r.isValid())
    {
        qCritical() << "Reply is not valid:" << r.error();
    }
}

QString DBusService::getName()
{
    QDBusInterface iAdapter(BLUEZ_SERVICE,
                            BLUEZ_PATH,
                            BLUEZ_ADAPTER,
                            QDBusConnection::systemBus());

    QDBusInterface iProperties(BLUEZ_SERVICE,
                               BLUEZ_PATH,
                               FREEDESKTOP_PROPERTIES,
                               QDBusConnection::systemBus());

    if (!iAdapter.isValid())
    {
        qCritical() << "Adapter is not valid";
        emit adapterNotValid();
        return QString("not defined");
    }
    if (!iProperties.isValid())
    {
        qCritical() << "Properties is not valid";
        emit adapterNotValid();
        return QString("not defined");
    }

    QList<QVariant> args;
    args << BLUEZ_ADAPTER << "Name";
    QDBusReply<QVariant> r = iProperties.callWithArgumentList(
                QDBus::CallMode::AutoDetect, "Get", args);

    if (!r.isValid())
    {
        qCritical() << "Reply is not valid:" << r.error();
        return QString();
    }
    return r.value().toString();
}

void DBusService::getPowered()
{
    QDBusInterface iAdapter(BLUEZ_SERVICE,
                            BLUEZ_PATH,
                            BLUEZ_ADAPTER,
                            QDBusConnection::systemBus());

    QDBusInterface iProperties(BLUEZ_SERVICE,
                               BLUEZ_PATH,
                               FREEDESKTOP_PROPERTIES,
                               QDBusConnection::systemBus());

    if (!iAdapter.isValid())
    {
        qCritical() << "Adapter is not valid";
        emit adapterNotValid();
        emit poweredChanged(false);
        return;
    }
    if (!iProperties.isValid())
    {

        qCritical() << "Properties is not valid";
        emit adapterNotValid();
        emit poweredChanged(false);
        return;
    }

    QList<QVariant> args;
    args << BLUEZ_ADAPTER << "Powered";

    QDBusPendingCall pendingReply =
            iProperties.asyncCallWithArgumentList("Get", args);

    QDBusPendingCallWatcher *watcher =
            new QDBusPendingCallWatcher(pendingReply);

    QObject::connect(watcher, SIGNAL(finished(QDBusPendingCallWatcher*)),
                    this, SLOT(onPoweredReceived(QDBusPendingCallWatcher*)));
}

QStringList DBusService::getDevicesPaths()
{
    QDBusInterface iIntrospectable(BLUEZ_SERVICE,
                                   BLUEZ_PATH,
                                   FREEDESKTOP_INTROSPECTABLE,
                                   QDBusConnection::systemBus());

    QList<QVariant> args;
    args << BLUEZ_SERVICE;
    QDBusReply<QString> r = iIntrospectable.callWithArgumentList(
                QDBus::CallMode::AutoDetect, "Introspect", args);

    if (!r.isValid())
    {
        qCritical() << "Reply is not valid" << r.error();
        return QStringList();
    }

    QDomDocument introspectOutput;
    introspectOutput.setContent(r);
    QDomNodeList nodes = introspectOutput.elementsByTagName("node");

     QStringList devicePaths;
    for (int i = 0; i < nodes.size(); ++i)
    {
        QString devicePath = nodes.at(i).toElement().attribute("name");
        if (!devicePath.isEmpty())
            devicePaths.push_back(devicePath);
    }

    return devicePaths;
}

QVariantMap DBusService::getAllDeviceProperties(QString devicePath)
{
    QDBusInterface iDevice(BLUEZ_SERVICE,
                           BLUEZ_PATH + '/' + devicePath,
                           BLUEZ_DEVICE, QDBusConnection::systemBus());
    QDBusInterface iProperties(BLUEZ_SERVICE, "/org/bluez/hci0/" + devicePath, FREEDESKTOP_PROPERTIES, QDBusConnection::systemBus());
    if (!iDevice.isValid())
    {
        qDebug() << "Adapter is not valid";
        emit adapterNotValid();
        return QVariantMap();
    }
    if (!iProperties.isValid())
    {
        qDebug() << "Properties is not valid";
        emit adapterNotValid();
        return QVariantMap();
    }

    QList<QVariant> args;
    args << BLUEZ_DEVICE;// << "Name";
    QDBusReply<QVariantMap> r = iProperties.callWithArgumentList(QDBus::CallMode::AutoDetect, "GetAll", args);

    if (!r.isValid())
    {
        qDebug() << "Reply in not valid:" << r.error();
    }
    return r;
}

void DBusService::connect(QString devicePath)
{
    qDebug() << metaObject()->className() << "\t - connectDevice" << devicePath;
    QDBusInterface iDevice(BLUEZ_SERVICE,
                           BLUEZ_PATH + '/' + devicePath,
                           BLUEZ_DEVICE,
                           QDBusConnection::systemBus());

    if (!iDevice.isValid())
    {
        qWarning() << metaObject()->className() <<
                      "\t - connectDevice():" <<
                      "Device interface is not available";
        return;
    }

    iDevice.call(QDBus::CallMode::AutoDetect, "Connect");
}

void DBusService::disconnect(QString devicePath)
{
    qDebug() << metaObject()->className() << "\t - diconnectDevice" << devicePath;
    QDBusInterface iDevice(BLUEZ_SERVICE,
                           BLUEZ_PATH + '/' + devicePath,
                           BLUEZ_DEVICE,
                           QDBusConnection::systemBus());

    if (!iDevice.isValid())
    {
        qDebug() << "Device interface is not valid";
        return;
    }

    iDevice.call(QDBus::CallMode::AutoDetect, "Disconnect");
}

void DBusService::pair(QString devicePath)
{
    QDBusInterface iDevice(BLUEZ_SERVICE,
                           BLUEZ_PATH + '/' + devicePath,
                           BLUEZ_DEVICE,
                           QDBusConnection::systemBus());

    if (!iDevice.isValid())
    {
        qCritical() << "Device interface is not valid";
        emit adapterNotValid();
        return;
    }

    currentPath = devicePath;
    QDBusPendingCall pendingCall = iDevice.asyncCall("Pair");
    QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(pendingCall);
    QObject::connect(watcher, SIGNAL(finished(QDBusPendingCallWatcher*)),
                    this, SLOT(onPairedChanged(QDBusPendingCallWatcher*)));
}

void DBusService::cancelPairing(QString devicePath)
{
    QDBusInterface iDevice(BLUEZ_SERVICE,
                           BLUEZ_PATH + '/' + devicePath,
                           BLUEZ_DEVICE,
                           QDBusConnection::systemBus());

    if (!iDevice.isValid())
    {
        qCritical() << "Device interface is not valid";
        return;
    }

    QDBusReply<QVariant> reply =
            iDevice.call(QDBus::CallMode::AutoDetect, "CancelPairing");

    if (!reply.isValid())
        qDebug() << "Reply in not valid:" << reply.error();
}

void DBusService::removeDevice(QString devicePath)
{
    QDBusInterface iAdapter(BLUEZ_SERVICE,
                            BLUEZ_PATH,
                            BLUEZ_ADAPTER,
                            QDBusConnection::systemBus());

    QDBusInterface iDevice(BLUEZ_SERVICE,
                           BLUEZ_PATH + '/' + devicePath,
                           BLUEZ_DEVICE,
                           QDBusConnection::systemBus());

    if (!iAdapter.isValid())
    {
        qCritical() << "Adapter is not valid";
    }
    if (!iDevice.isValid())
    {
        qCritical() << "Properties is not valid";
    }

    //QVariant deviceToRemove;

    QList<QVariant> args;
    args << QVariant::fromValue(QDBusObjectPath(iDevice.path()));
    QDBusReply<QVariant> r =
            iAdapter.callWithArgumentList(QDBus::CallMode::AutoDetect,
                                          "RemoveDevice",
                                          args);

    if (!r.isValid())
    {
        qCritical() << "Reply is not valid:" << r.error();
    }
}

void DBusService::getPaired(QString devicePath)
{
    QDBusInterface iDevice(BLUEZ_SERVICE,
                           BLUEZ_PATH + '/' + devicePath,
                           BLUEZ_DEVICE,
                           QDBusConnection::systemBus());

    QDBusInterface iProperties(BLUEZ_SERVICE,
                               BLUEZ_PATH + '/' + devicePath,
                               FREEDESKTOP_PROPERTIES,
                               QDBusConnection::systemBus());

    if (!iDevice.isValid())
    {
        qDebug() << "Device interface is not valid";
        return;
    }

    QList<QVariant> args;
    args << BLUEZ_DEVICE << "Paired";

    QDBusPendingCall pendingReply =
            iProperties.asyncCallWithArgumentList("Get", args);

    QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(pendingReply);
    QObject::connect(watcher, SIGNAL(finished(QDBusPendingCallWatcher*)),
                    this, SLOT(onPairedReceived(QDBusPendingCallWatcher*)));
}

bool DBusService::getConnected(QString devicePath)
{
    qDebug() << "Device path to connect: " << devicePath;
    QDBusInterface iDevice(BLUEZ_SERVICE,
                           BLUEZ_PATH + '/' + devicePath,
                           BLUEZ_DEVICE,
                           QDBusConnection::systemBus());

    QDBusInterface iProperties(BLUEZ_SERVICE,
                               BLUEZ_PATH + '/' + devicePath,
                               FREEDESKTOP_PROPERTIES,
                               QDBusConnection::systemBus());

    if (!iDevice.isValid())
    {
        qDebug() << "Device interface is not valid";
        return false;
    }

    QList<QVariant> args;
    args << BLUEZ_DEVICE << "Connected";
    QDBusReply<QVariant> r = iProperties.callWithArgumentList(
                QDBus::CallMode::AutoDetect, "Get", args);

    if (!r.isValid())
    {
        qDebug() << "Reply in not valid:" << r.error();
        return false;
    }
    return r.value().toBool();
}

void DBusService::setDeviceAlias(QString devicePath, QString alias)
{
    QDBusInterface iDevice(BLUEZ_SERVICE,
                           BLUEZ_PATH + '/' + devicePath,
                           BLUEZ_DEVICE,
                           QDBusConnection::systemBus());

    QDBusInterface iProperties(BLUEZ_SERVICE,
                               BLUEZ_PATH + '/' + devicePath,
                               FREEDESKTOP_PROPERTIES,
                               QDBusConnection::systemBus());

    if (!iDevice.isValid())
    {
        qDebug() << "Device interface is not valid";
        return;
    }

    QList<QVariant> args;
    args << BLUEZ_DEVICE << "Alias" << alias;
    QDBusReply<QVariant> r = iProperties.callWithArgumentList(
                QDBus::CallMode::AutoDetect, "Set", args);

    if (!r.isValid())
    {
        qDebug() << "Reply in not valid:" << r.error();
    }
}

void DBusService::onPoweredChanged(QDBusPendingCallWatcher * watcher)
{
    qDebug() << "Powered state changed.";
    if (watcher)
        delete watcher;

    getPowered();
}

void DBusService::onPoweredReceived(QDBusPendingCallWatcher *watcher)
{
    QDBusPendingReply<QVariant> reply = *watcher;

    qDebug() << "Pending finished.  Method: " << reply.argumentAt<0>();
    qDebug() << "Powered state answer:" << reply.value();
    if (reply.isError())
    {
        qDebug() << "Powered state error:" << reply.error();
    }
    else
    {
        bool isPowered = QVariant(reply.argumentAt<0>()).toBool();
        qDebug() << "Powered state received:" << isPowered;
        emit poweredChanged(isPowered);
    }
    watcher->deleteLater();
}

void DBusService::onPairedChanged(QDBusPendingCallWatcher *watcher)
{
    qDebug() << "Paired state changed.";
    if (watcher)
        delete watcher;

    getPaired(currentPath);
}

void DBusService::onPairedReceived(QDBusPendingCallWatcher *)
{
    // TBD with back
}

void DBusService::onConnectedChanged(QDBusPendingCallWatcher *)
{
    // TBD with back
}

void DBusService::onConnectedReceived(QDBusPendingCallWatcher *)
{
    // TBD with back
}

void DBusService::onDeviceInfoChanged(QDBusPendingCallWatcher *)
{
    // TBD with back
}

void DBusService::onDeviceInfoReceived(QDBusPendingCallWatcher *)
{
    // TBD with back
}

DBusService::DBusService(QObject *parent) : QObject(parent)
{
    // TBD with back
}

void DBusService::setPowered(bool value)
{
    QDBusInterface iAdapter(BLUEZ_SERVICE,
                            BLUEZ_PATH,
                            BLUEZ_ADAPTER,
                            QDBusConnection::systemBus());

    QDBusInterface iProperties(BLUEZ_SERVICE,
                               BLUEZ_PATH,
                               FREEDESKTOP_PROPERTIES,
                               QDBusConnection::systemBus());

    if (!iAdapter.isValid())
    {
        qWarning() << "Adapter interface is not valid";
        emit adapterNotValid();
        emit poweredChanged(false);
        return;
    }

    if (!iProperties.isValid())
    {
        qWarning() << "Properties interface is not valid";
        emit adapterNotValid();
        emit poweredChanged(false);
        return;
    }

    QList<QVariant> args;
    args << BLUEZ_ADAPTER << "Powered" << QVariant::fromValue(
                QDBusVariant(value));

    QDBusPendingCall pendingCall =
            iProperties.asyncCallWithArgumentList("Set", args);

    QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(pendingCall);

    QObject::connect(watcher, SIGNAL(finished(QDBusPendingCallWatcher*)),
                    this, SLOT(onPoweredChanged(QDBusPendingCallWatcher*)));
}
