#ifndef DEVICE_H
#define DEVICE_H

#include <QObject>
#include <QtGlobal>
#include <QQmlEngine>
#include <QtQml>
#include <memory>

class Device : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int deviceID
               READ deviceID
               WRITE setDeviceID
               NOTIFY deviceIDChanged)

    Q_PROPERTY(QString deviceName
               READ deviceName
               WRITE setDeviceName
               NOTIFY deviceNameChanged)

    Q_PROPERTY(DeviceStatus deviceStatus
               READ deviceStatus
               WRITE setDeviceStatus
               NOTIFY deviceStatusChanged)

    Q_PROPERTY(int batteryLevel
               READ batteryLevel
               WRITE setBatteryLevel
               NOTIFY batteryLevelChanged)

    Q_PROPERTY(bool isSaved
               READ isSaved
               WRITE setIsSaved
               NOTIFY isSavedChanged)

    Q_PROPERTY(QString modelName
               READ modelName
               WRITE setModelName
               NOTIFY modelNameChanged)

    Q_PROPERTY(QString serialNumber
               READ serialNumber
               WRITE setSerialNumber
               NOTIFY serialNumberChanged)

    Q_PROPERTY(QString version
               READ version
               WRITE setVersion
               NOTIFY versionChanged)

    Q_PROPERTY(QString modelNumber
               READ modelNumber
               WRITE setModelNumber
               NOTIFY modelNumberChanged)

    // PROPERTIES FROM ADAPTER:

    Q_PROPERTY(QString address
               READ address
               WRITE setAddress)

    Q_PROPERTY(QString addressType
               READ addressType
               WRITE setAddressType)

    Q_PROPERTY(QString alias
               READ alias
               WRITE setAlias)

    Q_PROPERTY(bool blocked
               READ blocked
               WRITE setBlocked)

    Q_PROPERTY(uint devClass
               READ devClass
               WRITE setDevClass)

    Q_PROPERTY(bool connected
               READ connected
               WRITE setConnected)

    Q_PROPERTY(QString icon
               READ icon
               WRITE setIcon)

    Q_PROPERTY(bool legacyPairing
               READ legacyPairing
               WRITE setLegacyPairing)

    Q_PROPERTY(QString modalias
               READ modalias
               WRITE setModalias)

    Q_PROPERTY(QString name
               READ name
               WRITE setName
               NOTIFY nameChanged)

    Q_PROPERTY(bool paired
               READ paired
               WRITE setPaired)

    Q_PROPERTY(bool servicesResolved
               READ servicesResolved
               WRITE setServicesResolved)

    Q_PROPERTY(bool trusted
               READ trusted
               WRITE setTrusted)

    Q_PROPERTY(QStringList UUIDs
               READ UUIDs
               WRITE setUUIDs)

public:

    enum DeviceStatus
    {
        Unpaired = Qt::UserRole + 1,
        Pairing,
        Paired,
        Connecting,
        Connected,
        Disconnecting,
        Unpairing,
    };
    Q_ENUM(DeviceStatus)

    static void declareQML()
    {
        qmlRegisterType<Device>("Device", 1, 0, "Device");
    }

    Device() : QObject() {}
    explicit Device(int id,
                    QString deviceName,
                    DeviceStatus status = Unpaired,
                    int batteryLevel = 0,
                    QObject *parent = nullptr);
    QString devicePath() const;
    void setDevicePath(const QString &newValue);

    int deviceID() const;
    void setDeviceID(int newID);
    QString deviceName() const;
    void setDeviceName(const QString& newDeviceName);
    DeviceStatus deviceStatus() const;
    void setDeviceStatus(DeviceStatus newStatus);
    int batteryLevel() const;
    void setBatteryLevel(int newLevel);
    bool isSaved() const;
    void setIsSaved(bool newValue);
    QString modelName() const;
    void setModelName(const QString& newValue);
    QString serialNumber() const;
    void setSerialNumber(const QString& newValue);
    QString version() const;
    void setVersion(const QString& newValue);
    QString modelNumber() const;
    void setModelNumber(const QString& newValue);

    // Adapter getters
    QString address() const;
    QString addressType() const;
    QString alias() const;
    bool blocked() const;
    uint devClass() const;
    bool connected() const;
    QString icon() const;
    bool legacyPairing() const;
    QString modalias() const;
    QString name() const;
    bool paired() const;
    bool servicesResolved() const;
    bool trusted() const;
    QStringList UUIDs() const;
    QString getBinaryDeviceClass();
    QString getGalaxyIconName();

    // Adapter setters;
    void setAddress(const QString &value);
    void setAddressType(const QString &value);
    void setAlias(const QString &value);
    void setBlocked(bool value);
    void setDevClass(uint value);
    void setConnected(bool value);
    void setIcon(const QString &value);
    void setLegacyPairing(bool value);
    void setModalias(const QString &value);
    void setName(const QString &value);
    void setPaired(bool value);
    void setServicesResolved(bool value);
    void setTrusted(bool value);
    void setUUIDs(const QStringList &value);

signals:

    void deviceIDChanged();
    void deviceNameChanged();
    void deviceStatusChanged();
    void batteryLevelChanged();
    void isSavedChanged();
    void modelNameChanged();
    void serialNumberChanged();
    void versionChanged();
    void modelNumberChanged();
    void nameChanged();

private:
    QString m_devicePath;
    int m_deviceID;
    QString m_deviceName;
    DeviceStatus m_deviceStatus;
    int m_batteryLevel;
    bool m_isSaved;
    QString m_modelName;
    QString m_serialNumber;
    QString m_version;
    QString m_modelNumber;

    // Adapter fields:
    QString m_address;
    QString m_addressType;
    QString m_alias;
    bool m_blocked;
    uint m_devClass;
    bool m_connected;
    QString m_icon;
    bool m_legacyPairing;
    QString m_modalias;
    QString m_name;
    bool m_paired;
    bool m_servicesResolved;
    bool m_trusted;
    QStringList m_UUIDs;

};

typedef std::shared_ptr<Device> DevicePtr;

#endif // DEVICE_H
