#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>

class Settings : public QObject
{
    Q_OBJECT

public:
    static Settings* getInstance();
    ~Settings();
    static bool isDemo;
    static int demoSimulationPeriod;
    static int devicesUpdatePeriod;
    static int demoDeviceQuantity;

public slots:
    void setDemoMode();


private:
    Settings();
    static Settings* m_instance;
};

#endif // SETTINGS_H
