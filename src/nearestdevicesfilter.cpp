#include "nearestdevicesfilter.h"
#include "devices.h"

NearestDevicesFilter::NearestDevicesFilter(QObject *parent) :
    QSortFilterProxyModel(parent)
{  
}

bool NearestDevicesFilter::filterAcceptsRow(
        int sourceRow,
        const QModelIndex &sourceParent) const
{
    bool isSaved =
            sourceModel()->index(sourceRow, 0, sourceParent).
            data(Devices::IsPairedRole).toBool();

    return !isSaved;
}
