#include "devices.h"
#include <random>
#include <QTimer>
#include "mydevices.h"
#include "dbusservice.h"
#include "settings.h"
#include <QDebug>
#include <QVariantMap>
#include <QRandomGenerator>

Devices* Devices::m_instance = nullptr;

Devices::Devices(QObject *parent):
    m_currentDevice(nullptr)
{
    this->setParent(parent);

    updateTimer.setInterval(Settings::devicesUpdatePeriod);
    updateTimer.setSingleShot(false);
    QObject::connect(&updateTimer, SIGNAL(timeout()), this, SLOT(update()));
    updateTimer.start();
}

Devices::~Devices()
{
    clearDevices();
    m_instance->deleteLater();
    m_instance = nullptr;
}

void Devices::update()
{ 
    if (!Settings::isDemo)
    {
        devices.clear();
        QStringList paths = DBusService::getInstance()->getDevicesPaths();

        for (auto path : paths)
        {
            qDebug() << metaObject()->className() << "\t - path:" << path;
            constructDevice(DBusService::getInstance()->getAllDeviceProperties(path));
        }
        DBusService::getInstance()->startDiscovery();
    }
    else
    {
        qWarning() << "Constructing fake devices";
        if (devices.isEmpty())
            constructFakeDevices();
    }
}

void Devices::stopUpdateTimer()
{
    updateTimer.stop();
}

void Devices::onPairingComplete()
{
    if (currentDevice()->deviceStatus() != Device::Pairing)
        return;

    beginResetModel();

    setIsPairing(false);
    currentDevice()->setDeviceStatus(Device::Paired);
    currentDevice()->setPaired(true);
    updateData();
    endResetModel();
}

void Devices::onDeviceRemoved()
{
    beginResetModel();
    setIsUnpairing(false);
    currentDevice()->setDeviceStatus(Device::Unpaired);
    currentDevice()->setPaired(false);
    updateData();
    endResetModel();
}

void Devices::disconnectAllDevices()
{
    beginResetModel();
    for (auto device : devices)
    {
        if (device->connected())
        {
            device->setConnected(false);
            device->setDeviceStatus(Device::Paired);
        }
    }
    updateData();
    endResetModel();
}

void Devices::selectDevice(int id)
{
    for (auto d : devices)
    {
        if (d->deviceID() == id)
        {
            setCurrentDevice(d);
        }
    }
}

bool Devices::isCurrentDeviceConnected()
{
    if (!currentDevice())
    {
        qDebug() << "No device!";
        return false;
    }

    qDebug() << currentDevice()->connected();
    return currentDevice()->connected();
}

QString Devices::currentDeviceSerialNumber()
{
    return currentDevice()->serialNumber();
}

QString Devices::currentDeviceName()
{
    return currentDevice()->deviceName();
}

QString Devices::currentDeviceVersion()
{
    return currentDevice()->version();
}

QString Devices::currentDeviceModel()
{
    return currentDevice()->modelNumber();
}

void Devices::addDevice(DevicePtr device)
{
    beginResetModel();
    devices.push_back(device);
    updateData();
    endResetModel();
}

bool Devices::isBusy() const
{
    return m_isBusy;
}

void Devices::setIsBusy(bool newValue)
{
    m_isBusy = newValue;
    emit isBusyChanged();
}

DevicePtr Devices::currentDevice() const
{
    return m_currentDevice;
}

void Devices::setCurrentDevice(DevicePtr newValue)
{
    m_currentDevice = newValue;
    emit currentDeviceChanged();
}

bool Devices::isPairing() const
{
    return m_isPairing;
}

void Devices::setIsPairing(bool newValue)
{
    m_isPairing = newValue;
    emit isPairingChanged();
}

bool Devices::isConnecting() const
{
    return m_isConnecting;
}

void Devices::setIsConnecting(bool newValue)
{
    m_isConnecting = newValue;
    emit isConnectingChanged();
}

DevicePtr Devices::constructDevice(QVariantMap deviceProperties)
{
    QVariantMap::Iterator propertyIterator = deviceProperties.begin();
    DevicePtr device {new Device()};

    device->setAddress(deviceProperties.value("Address").toString());
    device->setDevicePath(device->address().replace(":", "_").prepend("dev_"));
    device->setAddressType(deviceProperties.value("AddressType").toString());
    device->setAlias(deviceProperties.value("Alias").toString());
    device->setBlocked(deviceProperties.value("Blocked").toBool());
    device->setDevClass(deviceProperties.value("Class").toUInt());
    device->setConnected(deviceProperties.value("Connected").toBool());
    device->setIcon(deviceProperties.value("Icon").toString());
    device->setLegacyPairing(deviceProperties.value("LegacyPairing").toBool());
    device->setModalias(deviceProperties.value("Modalias").toString());
    device->setName(deviceProperties.value("Name").toString());
    device->setPaired(deviceProperties.value("Paired").toBool());
    device->setServicesResolved(deviceProperties.value("ServicesResolved").toBool());
    device->setTrusted(deviceProperties.value("Trusted").toBool());
    device->setUUIDs(deviceProperties.value("UUIDs").toStringList());

    device->setDeviceID(devices.size()); // ID is necessary to operate via qml table
    qDebug() << device->deviceID();
    addDevice(device);

    qDebug() << deviceProperties;
}

void Devices::constructFakeDevices()
{
    QMultiMap<QString, uint32_t> possibleAlias =
    {
        {"W520 Tower PC",   256},            // 000000000000000100000000 Computer
        {"F+",              268},            // 000000000000000100001100 Laptop
        {"Honor H50",       512},            // 000000000000001000000000 Phone
        {"MTS music",       1044},           // 000000000000010000010100 Speaker
        {"LG50",            1080},           // 000000000000010000111000 Tv
        {"Samsumg Flat",    1084},           // 000000000000010000111100 Tv
        {"GPods-130",       1024},           // 000000000000010000000000 Headphones
        {"Logitech K100",   1344},           // 000000000000010101000000 Keyboard
        {"Logitech M171",   1472},           // 000000000000010111000000 Mouse
        {"XBox universe",   1280},           // 000000000000010100000000 GamePad
        {"LG75",            1552}           // 000000000000011000010000 Tv
    };
    auto deviceBaseIt = possibleAlias.begin();

    for (int i = 0; i < Settings::demoDeviceQuantity; ++i)
    {
        DevicePtr device {new Device()};

        if (deviceBaseIt == possibleAlias.end())
            deviceBaseIt = possibleAlias.begin();

        device->setAlias(deviceBaseIt.key());
        device->setDeviceName(deviceBaseIt.key());
        device->setDevClass(deviceBaseIt.value());
        device->setAddress("AA:01:02:03:" + QString::number(i));
        device->setDevicePath("AA:01:02:03:" + QString::number(i));
        device->setAddressType("public");
        device->setBlocked(false);
        device->setConnected(false);
        device->setIcon(QString());
        device->setLegacyPairing(false);
        device->setModalias(QString());
        device->setPaired(false);
        device->setServicesResolved(false);
        device->setTrusted(true);
        device->setUUIDs(QStringList());
        device->setBatteryLevel(QRandomGenerator::global()->bounded(5, 101));
        device->setSerialNumber(
                    QString::number(QRandomGenerator::global()->bounded(100, 999)) +
                    "-" +
                    QString::number(QRandomGenerator::global()->bounded(100, 999)) +
                    "-" +
                    QString::number(QRandomGenerator::global()->bounded(100, 999))
                    );
        device->setModelNumber(
                    deviceBaseIt.key() +
                    "-" +
                    QString::number(QRandomGenerator::global()->bounded(100, 999)));
        device->setVersion(
                    QString::number(QRandomGenerator::global()->bounded(1, 100)) +
                    "." +
                    QString::number(QRandomGenerator::global()->bounded(1, 100)));

        device->setDeviceID(i); // ID is necessary to operate via qml table

        addDevice(device);
        deviceBaseIt++;
    }
}

bool Devices::isUnpairing() const
{
    return m_isUnpairing;
}

void Devices::setIsUnpairing(bool value)
{
    m_isUnpairing = value;
    emit isUnpairingChanged();
}

bool Devices::isDisconnecting() const
{
    return m_isDisconnecting;
}

void Devices::setIsDisconnecting(bool value)
{
    m_isDisconnecting = value;
    emit isDisconnectingChanged();
}

void Devices::discoverDevices()
{
    // For DEMO only:
    qDebug() << "discoverDevices()";
    setIsBusy(true);
}

void Devices::clearDevices()
{
    beginResetModel();
    devices.clear();
    updateData();
    endResetModel();
}

void Devices::connect(int id)
{
    beginResetModel();
    selectDevice(id);
    setIsConnecting(true);
    currentDevice()->setDeviceStatus(Device::Connecting);
    updateData();
    endResetModel();

    if (!Settings::isDemo)
        DBusService::getInstance()->connect(currentDevice()->devicePath());
    else
        QTimer::singleShot(Settings::demoSimulationPeriod,
                           this,
                           &Devices::onConnected);
}

void Devices::disconnect(int id)
{
    beginResetModel();
    selectDevice(id);
    setIsDisconnecting(true);
    currentDevice()->setDeviceStatus(Device::Disconnecting);
    updateData();
    endResetModel();

    if (!Settings::isDemo)
        DBusService::getInstance()->connect(currentDevice()->devicePath());
    else
        QTimer::singleShot(Settings::demoSimulationPeriod,
                           this,
                           &Devices::onDisconnected);
}

QString Devices::currentDeviceAlias()
{
    if (!m_currentDevice)
        return QString();

    return currentDevice()->alias();
}

void Devices::setDeviceAlias(QString alias)
{
    if (alias.simplified().isEmpty())
        alias = "not set";
    if (!currentDevice()) return;

    beginResetModel();

    if (!Settings::isDemo)
        DBusService::getInstance()->setDeviceAlias(
                     currentDevice()->devicePath(),
                     alias);
    else
        currentDevice()->setAlias(alias);
    updateData();
    endResetModel();
}

void Devices::pair(int id)
{
    selectDevice(id);
    beginResetModel();
    setIsPairing(true);
    currentDevice()->setDeviceStatus(Device::Pairing);
    updateData();
    endResetModel();

    if (!Settings::isDemo)
        DBusService::getInstance()->pair(currentDevice()->devicePath());
    else
        QTimer::singleShot(Settings::demoSimulationPeriod, this, &Devices::onPairingComplete);
}

Device::DeviceStatus Devices::getDeviceStatus(int id)
{
    selectDevice(id);

    return currentDevice()->deviceStatus();
}

void Devices::cancelPairing()
{
    if (!currentDevice())
        return;

    qDebug() << metaObject()->className() <<
                "\t - cancel pairing" <<
                currentDevice()->devicePath();
    beginResetModel();
    DBusService::getInstance()->cancelPairing(currentDevice()->devicePath());
    updateData();
    endResetModel();
}

void Devices::removeDevice()
{
    if (!currentDevice())
        return;

    beginResetModel();
    setIsUnpairing(true);
    currentDevice()->setDeviceStatus(Device::Unpairing);
    currentDevice()->setAlias(currentDevice()->deviceName());
    if (!Settings::isDemo)
        DBusService::getInstance()->removeDevice(currentDevice()->devicePath());
    else
        QTimer::singleShot(Settings::demoSimulationPeriod, this, &Devices::onDeviceRemoved);
    updateData();
    endResetModel();
}

void Devices::discardPairing()
{
    setIsPairing(false);

    qDebug() << "canceled";
    currentDevice()->setDeviceStatus(Device::Unpaired);
    updateData();
    endResetModel();
}

Devices *Devices::getInstance()
{
    if (!m_instance)
        m_instance = new Devices();

    return m_instance;
}

int Devices::rowCount(const QModelIndex &parent) const
{
    return devices.size();
}

QVariant Devices::data(const QModelIndex &index, int role) const
{
    auto row = index.row();
    if (row < 0 || row >= devices.size())
        return QVariant();

    DevicePtr device = devices.at(row);
    switch (role)
    {
        case IdRole:
            return device->deviceID();
        case AliasRole:
            return device->alias();
        case IconRole:
            return device->getGalaxyIconName();
        case DevClassRole:
            return device->getBinaryDeviceClass();
        case NameRole:
            return device->name();
        case StatusRole:
            return device->deviceStatus();
        case BatteryLevelRole:
            return device->batteryLevel();
        case IsPairedRole:
            return device->paired();
        case IsSavedRole:
            return device->isSaved();
        default:
            return QVariant();
    }
}

QHash<int, QByteArray> Devices::roleNames() const
{
    QHash<int, QByteArray> roleNames;
    roleNames[IdRole] = QByteArrayLiteral("id");
    roleNames[AliasRole] = QByteArrayLiteral("deviceAlias");
    roleNames[IconRole] = QByteArrayLiteral("icon");
    roleNames[DevClassRole] = QByteArrayLiteral("deviceClass");
    roleNames[NameRole] = QByteArrayLiteral("name");
    roleNames[StatusRole] = QByteArrayLiteral("deviceStatus");
    roleNames[BatteryLevelRole] = QByteArrayLiteral("batteryLevel");
    roleNames[IsPairedRole] = QByteArrayLiteral("isPaired");
    roleNames[IsSavedRole] = QByteArrayLiteral("isSaved");
    return roleNames;
}

void Devices::onConnected()
{
    beginResetModel();
    setIsConnecting(false);
    currentDevice()->setDeviceStatus(Device::Connected);
    currentDevice()->setConnected(true);
    updateData();
    endResetModel();
}

void Devices::onDisconnected()
{
    beginResetModel();
    setIsDisconnecting(false);
    currentDevice()->setDeviceStatus(Device::Paired);
    currentDevice()->setConnected(false);
    updateData();
    endResetModel();
}

void Devices::updateData()
{
    QModelIndex start = this->index(0, 0);
    QModelIndex end = this->index(this->rowCount() - 1, 0);
    emit dataChanged(start, end);
}

QString Devices::getDevicePathById(int id)
{
    for (DevicePtr device : devices)
    {
        if (device->deviceID() == id)
            return device->devicePath();
    }
    return QString();
}
