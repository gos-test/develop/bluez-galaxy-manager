#ifndef NEARESTDEVICESFILTER_H
#define NEARESTDEVICESFILTER_H

#include <QSortFilterProxyModel>

class NearestDevicesFilter : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit NearestDevicesFilter(QObject *parent = nullptr);

protected:
    bool filterAcceptsRow(int sourceRow,
                          const QModelIndex &sourceParent) const;
};

#endif // NEARESTDEVICESFILTER_H
