#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QLocale>
#include <QTranslator>
#include <QQuickView>
#include <QQmlContext>
#include <QList>
#include "adapter.h"
#include "devices.h"
#include "settings.h"
#include "mydevicesfilter.h"
#include "nearestdevicesfilter.h"
#include "device.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    // Translation module:
    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "BluezManager_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            app.installTranslator(&translator);
            break;
        }
    }

    QQmlApplicationEngine engine;
    Settings::getInstance(); // settings signal connection init
    Device::declareQML();
    Adapter::declareQML();

    Adapter* adapter = new Adapter();

    MyDevicesFilter *myDevicesFilter = new MyDevicesFilter();
    myDevicesFilter->setSourceModel(Devices::getInstance());

    NearestDevicesFilter *nearestDevicesFilter = new NearestDevicesFilter();
    nearestDevicesFilter->setSourceModel(Devices::getInstance());

    engine.rootContext()->setContextProperty("allDevices", Devices::getInstance());
    engine.rootContext()->setContextProperty("myDevices", myDevicesFilter);
    engine.rootContext()->setContextProperty("nearestDevices", nearestDevicesFilter);
    engine.rootContext()->setContextProperty("adapter", adapter);

    const QUrl url(QStringLiteral("qrc:/frontend/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
