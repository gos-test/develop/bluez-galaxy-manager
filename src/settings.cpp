#include "settings.h"
#include "dbusservice.h"
#include "devices.h"

bool Settings::isDemo {false};
int Settings::devicesUpdatePeriod {10000};
int Settings::demoSimulationPeriod {1000};
int Settings::demoDeviceQuantity {7};

Settings* Settings::m_instance = nullptr;

Settings *Settings::getInstance()
{
    if (!m_instance)
        m_instance = new Settings();

    return m_instance;
}

Settings::~Settings()
{
    if (m_instance)
        delete m_instance;

    m_instance = nullptr;
}

void Settings::setDemoMode()
{
    if (!isDemo)
    {
        qWarning() << "Switching to DEMO MODE";
        isDemo = true;
        Devices::getInstance()->stopUpdateTimer();
    }
}

Settings::Settings()
{
    connect(DBusService::getInstance(), SIGNAL(adapterNotValid()), this, SLOT(setDemoMode()));
}
