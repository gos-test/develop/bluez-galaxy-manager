#ifndef ADAPTER_H
#define ADAPTER_H

#include <QObject>
#include <QtGlobal>
#include <QQmlEngine>
#include <QtQml>
#include <QtDBus/QtDBus>

class Adapter : public QObject
{
    Q_OBJECT

    Q_PROPERTY(AdapterStatus status
               READ status
               WRITE setStatus
               NOTIFY statusChanged)

    Q_PROPERTY(QString name
               READ name
               WRITE setName
               NOTIFY receiverNameChanged)
public:

    enum AdapterStatus
    {
        IsOff = Qt::UserRole + 1,
        IsSwitchingOn,
        IsOn,
        IsSwitchingOff,
        IsUnavailable
    };
    Q_ENUM(AdapterStatus)

    static void declareQML();

    explicit Adapter(QObject *parent = nullptr);
    ~Adapter();

    Q_INVOKABLE void switchOn();
    Q_INVOKABLE void switchOff();

    AdapterStatus status() const;
    void setStatus(AdapterStatus newStatus);
    QString name() const;
    void setName(const QString& newName);

signals:
    void statusChanged();
    void receiverNameChanged();

public slots:
    void onPoweredChanged(bool powered);
    void onSwitchOn();
    void onSwitchOff();

private:
    AdapterStatus m_status;
    QString m_name;
};

#endif // ADAPTER_H
