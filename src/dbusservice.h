#ifndef DBUSSERVICE_H
#define DBUSSERVICE_H

#include <QObject>
#include <QtDBus/QtDBus>

class Adapter;

class DBusService : public QObject
{
    Q_OBJECT
public:
    static DBusService* getInstance(QObject* parent = nullptr);
    virtual ~DBusService();

signals:
    void poweredChanged(bool powered);
    void adapterNotValid();

public slots:
    void startDiscovery();
    void stopDiscovery();
    QString getName();

    // Power slots:
    void setPowered(bool value);
    void getPowered();

    // Pair slots:
    void pair(QString devicePath);
    void cancelPairing(QString devicePath);
    void removeDevice(QString devicePath);
    void getPaired(QString devicePath);

    // Connect slots:
    void connect(QString devicePath);
    void disconnect(QString devicePath);
    bool getConnected(QString devicePath);

    // Device info slots:
    void setDeviceAlias(QString devicePath, QString alias);
    QVariantMap getAllDeviceProperties(QString devicePath);
    QStringList getDevicesPaths();

private slots:

    // Power slots:
    void onPoweredChanged(QDBusPendingCallWatcher*);
    void onPoweredReceived(QDBusPendingCallWatcher*);

    // Pair slots:
    void onPairedChanged(QDBusPendingCallWatcher*);
    void onPairedReceived(QDBusPendingCallWatcher*);

    // Connect slots:
    void onConnectedChanged(QDBusPendingCallWatcher*);
    void onConnectedReceived(QDBusPendingCallWatcher*);

    // Device info slots:
    void onDeviceInfoChanged(QDBusPendingCallWatcher*);
    void onDeviceInfoReceived(QDBusPendingCallWatcher*);

private:
    explicit DBusService(QObject *parent = nullptr);
    static DBusService* instance;
    QString currentPath;
};

#endif // DBUSSERVICE_H
