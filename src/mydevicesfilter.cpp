#include "mydevicesfilter.h"
#include "devices.h"

MyDevicesFilter::MyDevicesFilter(QObject *parent) :
    QSortFilterProxyModel(parent)
{
}

bool MyDevicesFilter::filterAcceptsRow(
        int sourceRow,
        const QModelIndex &sourceParent) const
{
    bool isSaved =
            sourceModel()->index(sourceRow, 0, sourceParent).
            data(Devices::IsPairedRole).toBool();

    return isSaved;
}
