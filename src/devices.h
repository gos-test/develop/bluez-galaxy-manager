#ifndef DEVICES_H
#define DEVICES_H

#include <QObject>
#include <QAbstractListModel>
#include <QList>
#include "device.h"

class Devices : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(bool isBusy
               READ isBusy
               WRITE setIsBusy
               NOTIFY isBusyChanged)

    Q_PROPERTY(bool isPairing
               READ isPairing
               WRITE setIsPairing
               NOTIFY isPairingChanged)

    Q_PROPERTY(bool isUnpairing
               READ isUnpairing
               WRITE setIsUnpairing
               NOTIFY isUnpairingChanged)

    Q_PROPERTY(bool isConnecting
               READ isConnecting
               WRITE setIsConnecting
               NOTIFY isConnectingChanged)

    Q_PROPERTY(bool isDisconnecting
               READ isDisconnecting
               WRITE setIsDisconnecting
               NOTIFY isDisconnectingChanged)

    Q_PROPERTY(DevicePtr currentDevice
               READ currentDevice
               WRITE setCurrentDevice
               NOTIFY currentDeviceChanged)

public:
    enum DeviceRoles
    {
        IdRole = Qt::UserRole + 1,
        AliasRole,
        DevClassRole,
        IconRole,
        NameRole,
        StatusRole,
        BatteryLevelRole,
        IsPairedRole,
        IsSavedRole
    };
    Q_ENUM(DeviceRoles)

    Q_INVOKABLE void discoverDevices();
    Q_INVOKABLE void clearDevices();
    Q_INVOKABLE void connect(int deviceId);
    Q_INVOKABLE void disconnect(int deviceId);
    Q_INVOKABLE void setDeviceAlias(QString alias);
    Q_INVOKABLE void pair(int id);
    Q_INVOKABLE Device::DeviceStatus getDeviceStatus(int id);
    Q_INVOKABLE void selectDevice(int id);
    Q_INVOKABLE void cancelPairing();
    Q_INVOKABLE void removeDevice();
    Q_INVOKABLE void discardPairing();
    Q_INVOKABLE QString currentDeviceAlias();
    Q_INVOKABLE bool isCurrentDeviceConnected();
    Q_INVOKABLE QString currentDeviceSerialNumber();
    Q_INVOKABLE QString currentDeviceName();
    Q_INVOKABLE QString currentDeviceVersion();
    Q_INVOKABLE QString currentDeviceModel();

    static Devices* getInstance();
    ~Devices();

    QList<DevicePtr> devices;

    void addDevice(DevicePtr);
    bool isBusy() const;
    void setIsBusy(bool newValue);
    DevicePtr currentDevice() const;
    void setCurrentDevice(DevicePtr newValue);
    bool isPairing() const;
    void setIsPairing(bool newValue);
    bool isConnecting() const;
    void setIsConnecting(bool newValue);
    DevicePtr constructDevice(QVariantMap deviceProperties);
    void constructFakeDevices();
    bool isUnpairing() const;
    void setIsUnpairing(bool value);
    bool isDisconnecting() const;
    void setIsDisconnecting(bool value);

    // QAbstractItemModel interface:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

signals:
    void isBusyChanged();
    void currentDeviceChanged();
    void isPairingChanged();
    void isConnectingChanged();
    void isUnpairingChanged();
    void isDisconnectingChanged();

public slots:
        void update();
        void stopUpdateTimer();
        void onPairingComplete();
        void onDeviceRemoved();
        void disconnectAllDevices();
private slots:
    void onConnected();
    void onDisconnected();

private:
    void updateData();

    bool m_isBusy {false};
    bool m_isPairing {false};
    bool m_isUnpairing {false};
    bool m_isConnecting {false};
    bool m_isDisconnecting {false};
    DevicePtr m_currentDevice;
    QString getDevicePathById(int id);
    QTimer updateTimer;

    explicit Devices(QObject *parent = nullptr);
    static Devices* m_instance;
};

#endif // DEVICES_H
