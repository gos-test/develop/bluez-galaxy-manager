#ifndef MYDEVICESMODEL_H
#define MYDEVICESMODEL_H

#include <QAbstractListModel>
#include <QHash>
#include <QList>
#include <memory>

class Device;
typedef std::shared_ptr<Device> DevicePtr;
class MyDevicesModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(bool isPairing
               READ isPairing
               WRITE setIsPairing
               NOTIFY isPairingChanged)

    Q_PROPERTY(DevicePtr currentDevice
               READ currentDevice
               WRITE setCurrentDevice
               NOTIFY currentDeviceChanged)

public:
    enum DeviceRoles
    {
        IdRole = Qt::UserRole + 1,
        AliasRole,
        NameRole,
        StatusRole,
        BatteryLevelRole
    };

    Q_ENUM(DeviceRoles)

    static MyDevicesModel* getInstance();
    ~MyDevicesModel();

    QHash<int, QByteArray> roleNames() const override;

    QList<DevicePtr> devices;
    int currentIndex {0};

    Q_INVOKABLE void connectDevice(int id);
    Q_INVOKABLE void disconnectDevice(int id);
    Q_INVOKABLE QString getDeviceName(int id);
    Q_INVOKABLE void setDeviceName(int id, QString newName);
    Q_INVOKABLE int getDeviceStatus(int id);
    Q_INVOKABLE void forgetDevice(int id);
    Q_INVOKABLE int getCount();
    Q_INVOKABLE void selectDevice(int id);
    Q_INVOKABLE int currentDeviceId();
    Q_INVOKABLE QString currentDeviceName();
    Q_INVOKABLE QString currentModelName();
    Q_INVOKABLE QString currentSerialNumber();
    Q_INVOKABLE QString currentVersion();
    Q_INVOKABLE QString currentModelNumber();
    Q_INVOKABLE int currentDeviceStatus();

    void addDevice(DevicePtr);
    DevicePtr currentDevice() const;
    void setCurrentDevice(DevicePtr newDevice);
    bool isPairing() const;
    void setIsPairing(bool newValue);
    bool isConnecting() const;
    void setIsConnecting(bool newValue);

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const override;

signals:
    void currentDeviceChanged();
    void isPairingChanged();

private slots:
    // For DEMO -----------------------------------------------------------
    void onDeviceConnected();
    void onDeviceDisconnected();
    // --------------------------------------------------------------------

private:
    explicit MyDevicesModel(QObject *parent = nullptr);
    DevicePtr m_currentDevice;
    bool m_isPairing {false};
    bool m_isConnecting {false};
    bool m_toForget {false};
    static MyDevicesModel* instance;
};

#endif // NEARESTDEVICESMODEL_H
