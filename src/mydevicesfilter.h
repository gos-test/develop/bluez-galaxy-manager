#ifndef MYDEVICESFILTER_H
#define MYDEVICESFILTER_H

#include <QSortFilterProxyModel>

class MyDevicesFilter : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit MyDevicesFilter(QObject *parent = nullptr);

protected:
    bool filterAcceptsRow(int sourceRow,
                          const QModelIndex &sourceParent) const;
};

#endif // MYDEVICESFILTER_H
