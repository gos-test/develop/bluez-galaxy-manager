#include "device.h"
#include <QDebug>
#include <QTimer>

Device::Device(int id,
               QString deviceName,
               DeviceStatus status,
               int batteryLevel,
               QObject *parent)
    : m_deviceID(id),
      m_deviceName(deviceName),
      m_deviceStatus(status),
      m_batteryLevel(batteryLevel),
      QObject{parent}
{
    qDebug() << "Device" << id << "is created";
}

QString Device::devicePath() const
{
    return m_devicePath;
}

void Device::setDevicePath(const QString &newValue)
{
    m_devicePath = newValue;
}

int Device::deviceID() const
{
    return m_deviceID;
}

void Device::setDeviceID(int newID)
{
    m_deviceID = newID;
    emit deviceIDChanged();
}

QString Device::deviceName() const
{
    return m_deviceName;
}

void Device::setDeviceName(const QString &newDeviceName)
{
    m_deviceName = newDeviceName;
    qDebug() << "Device name is changed on:" << newDeviceName;
    emit deviceNameChanged();
}

Device::DeviceStatus Device::deviceStatus() const
{
    return m_deviceStatus;
}

void Device::setDeviceStatus(DeviceStatus newStatus)
{
    m_deviceStatus = newStatus;
    emit deviceStatusChanged();
}

int Device::batteryLevel() const
{
    return m_batteryLevel;
}

void Device::setBatteryLevel(int newLevel)
{
    m_batteryLevel = newLevel;
    emit batteryLevelChanged();
}

bool Device::isSaved() const
{
    return m_isSaved;
}

void Device::setIsSaved(bool newValue)
{
    m_isSaved = newValue;
    emit isSavedChanged();
}

QString Device::modelName() const
{
    return m_modelName;
}

void Device::setModelName(const QString &newValue)
{
    m_modelName = newValue;
    emit modelNameChanged();
}

QString Device::serialNumber() const
{
    return m_serialNumber;
}

void Device::setSerialNumber(const QString &newValue)
{
    m_serialNumber = newValue;
    emit serialNumberChanged();
}

QString Device::version() const
{
    return m_version;
}

void Device::setVersion(const QString &newValue)
{
    m_version = newValue;
    emit versionChanged();
}

QString Device::modelNumber() const
{
    return m_modelNumber;
}

void Device::setModelNumber(const QString &newValue)
{
    m_modelNumber = newValue;
    emit modelNumberChanged();
}

QString Device::address() const
{
    return m_address;
}

QString Device::addressType() const
{
    return m_addressType;
}

QString Device::alias() const
{
    return m_alias;
}

bool Device::blocked() const
{
    return m_blocked;
}

uint Device::devClass() const
{
    return m_devClass;
}

bool Device::connected() const
{
    return m_connected;
}

QString Device::icon() const
{
    return m_icon;
}

bool Device::legacyPairing() const
{
    return m_legacyPairing;
}

QString Device::modalias() const
{
    return m_modalias;
}

QString Device::name() const
{
    return m_name;
}

bool Device::paired() const
{
    return m_paired;
}

bool Device::servicesResolved() const
{
    return m_servicesResolved;
}

bool Device::trusted() const
{
    return m_trusted;
}

QStringList Device::UUIDs() const
{
    return m_UUIDs;
}

QString Device::getBinaryDeviceClass()
{
    QString myStringOfBits( QString::number( devClass(), 2 ) );
    return myStringOfBits;
}

QString Device::getGalaxyIconName()
{
    QString classBinary = getBinaryDeviceClass();
    int length = classBinary.length();
    if (length < 13)
    {
        for (int i = 0; i < 13 - length; ++i)
        {
            classBinary.push_front(QChar('0'));
        }
    }

    QString::iterator it = classBinary.begin();
    QList<int> bits;
    while (it != classBinary.end())
    {
        if (it->toLatin1() == '1')
            bits.push_front(1);
        else
            bits.push_front(0);
        ++it;
    }

    // Computer
    if (bits.at(8) &&
       !bits.at(9) && !bits.at(10) && !bits.at(11) && !bits.at(12))
    {
        if (bits.at(2) && bits.at(3) &&
           !bits.at(4) && !bits.at(5) && !bits.at(6) && !bits.at(7))
            return QLatin1String("laptop");
        else
            return QLatin1String("computer");
    }

    // Phone
    if (bits.at(9) &&
       !bits.at(8) && !bits.at(10) && !bits.at(11) && !bits.at(12))
        return QLatin1String("phone");

    // Audio
    if (bits.at(10) &&
       !bits.at(8) && !bits.at(9) && !bits.at(11) && !bits.at(12))
    {
        if (bits.at(2) && bits.at(4) &&
           !bits.at(3) && !bits.at(5) && !bits.at(6) && !bits.at(7))
            return QLatin1String("speaker");

        if (bits.at(3) && bits.at(4) && bits.at(5) &&
           !bits.at(2) && !bits.at(6) && !bits.at(7))
            return QLatin1String("tv");

        if (bits.at(2) && bits.at(3) && bits.at(4) && bits.at(5) &&
           !bits.at(6) && !bits.at(7))
            return QLatin1String("tv");

        return QLatin1String("headphones");
    }

    // Peripheral
    if (bits.at(8) && bits.at(10) &&
       !bits.at(9) && !bits.at(11) && !bits.at(12))
    {
        if (bits.at(6) &&
           !bits.at(7))
            return QLatin1String("keyboard");

        if (bits.at(6) || bits.at(7))
            return QLatin1String("mouse");

        return QLatin1String("gamepad");
    }

    // Imaging
    if (bits.at(4) && bits.at(9) && bits.at(10) &&
       !bits.at(11) && !bits.at(12))
        return QLatin1String("tv");

    return QLatin1String("other");
}

void Device::setAddress(const QString &value)
{
    m_address = value;
}

void Device::setAddressType(const QString &value)
{
    m_addressType = value;
}

void Device::setAlias(const QString &value)
{
    m_alias = value;
}

void Device::setDevClass(uint value)
{
    m_devClass = value;
}

void Device::setBlocked(bool value)
{
    m_blocked = value;
}

void Device::setConnected(bool value)
{
    m_connected = value;
}

void Device::setIcon(const QString &value)
{
    m_icon = value;
}

void Device::setLegacyPairing(bool value)
{
    m_legacyPairing = value;
}

void Device::setModalias(const QString &value)
{
    m_modalias = value;
}

void Device::setName(const QString &value)
{
    m_name = value;
    emit nameChanged();
}

void Device::setPaired(bool value)
{
    m_paired = value;
}

void Device::setServicesResolved(bool value)
{
    m_servicesResolved = value;
}

void Device::setTrusted(bool value)
{
    m_trusted = value;
}

void Device::setUUIDs(const QStringList &value)
{
    m_UUIDs = value;
}
