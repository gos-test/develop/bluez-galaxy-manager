# BlueZ Galaxy Manager



## Description

GUI for BlueZ (target version 5.56).
Includes **demo** simulation mode in case if bluez interafaces are unavailable. Demo mode is enabled automatically if org.bluez contains no intefaces or absent.

## Demo mode

Devices data model is automatically filling up with fake devices for demonstration of main functionality of front-end. In that case no back-end commands are sending through the d-bus.

**My devices** section is not saving devices in demo in any file. All saved devices after restart will be deleted.

## Description of GUI
### - Adapter section

Allows you to switch on and off the adapter.

### - My devices section

Contains 'myDevices' model which is factually 'allDevices' model, filtered by 'paired = true' criteria.
All devices here are automatically saved after pairing in bluez backend and requires no saving through the front.

### - Nearest devices section

Contains 'nearestDevices' model which is factually 'allDevices' model, filtered by 'paired = false' criteria.
All devices here are received from 'Adapter1' interface of bluez.

