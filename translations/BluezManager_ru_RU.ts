<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AdapterSection</name>
    <message>
        <location filename="../frontend/AdapterSection.qml" line="32"/>
        <source>Мои сети</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/AdapterSection.qml" line="59"/>
        <source>Bluetooth</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModalConfirmation</name>
    <message>
        <location filename="../frontend/ModalConfirmation.qml" line="45"/>
        <source>Это устройство не подключится снова автоматически.
Потом вам придется подключать его заново.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/ModalConfirmation.qml" line="63"/>
        <source>Отменить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/ModalConfirmation.qml" line="77"/>
        <source>Забыть устройство</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModalInfo</name>
    <message>
        <location filename="../frontend/ModalInfo.qml" line="12"/>
        <location filename="../frontend/ModalInfo.qml" line="22"/>
        <source>Подключить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/ModalInfo.qml" line="14"/>
        <source>Имя</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/ModalInfo.qml" line="15"/>
        <source>Название модели</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/ModalInfo.qml" line="16"/>
        <source>Серийный номер</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/ModalInfo.qml" line="17"/>
        <source>Версия</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/ModalInfo.qml" line="18"/>
        <source>Номер модели</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/ModalInfo.qml" line="20"/>
        <source>Забыть это устройство</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/ModalInfo.qml" line="23"/>
        <source>Готово</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModalPairing</name>
    <message>
        <location filename="../frontend/ModalPairing.qml" line="32"/>
        <source>Требуется подтверждение подключаемого устройства</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/ModalPairing.qml" line="72"/>
        <source>Отменить</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyDevice</name>
    <message>
        <location filename="../frontend/MyDevice.qml" line="14"/>
        <source>Отключено</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/MyDevice.qml" line="15"/>
        <source>Подключено</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/MyDevice.qml" line="16"/>
        <source>Отключение</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/MyDevice.qml" line="17"/>
        <source>Подключение</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/MyDevice.qml" line="18"/>
        <source>Подключить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/MyDevice.qml" line="19"/>
        <source>Отключить</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyDevicesList</name>
    <message>
        <location filename="../frontend/MyDevicesList.qml" line="60"/>
        <source>Устройства отсутствуют</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyDevicesSection</name>
    <message>
        <location filename="../frontend/MyDevicesSection.qml" line="23"/>
        <source>Мои устройства</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NearestDevice</name>
    <message>
        <location filename="../frontend/NearestDevice.qml" line="94"/>
        <source>Подключить</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NearestDevicesList</name>
    <message>
        <location filename="../frontend/NearestDevicesList.qml" line="61"/>
        <source>Устройства отсутствуют</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NearestDevicesSection</name>
    <message>
        <location filename="../frontend/NearestDevicesSection.qml" line="32"/>
        <source>Ближайшие устройства</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../frontend/main.qml" line="19"/>
        <source>BlueZ Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/main.qml" line="175"/>
        <location filename="../frontend/main.qml" line="196"/>
        <source>Подключить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/main.qml" line="180"/>
        <location filename="../frontend/main.qml" line="194"/>
        <source>Отключить</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
